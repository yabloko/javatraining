package com.training.metadatareader;

/**
 * Created by andrew on 18.12.14.
 */
public class ReadData {
    public static void main(String[] args) {
        MetaDataReader reader = new TarMetaDataReader();
        String path = "/home/andrew/Downloads/document.tar";
        try {
            for (MetaData data : reader.getMetaData(path)) {
                System.out.println(data.getFieldName() + " -> " + data.getFieldValue());
            }
        }
        catch(WrongFormatException e) {
            e.getMessage();
        }
    }
}
