package com.training.metadatareader;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by andrew on 18.12.14.
 */
public class TarMetaDataReader implements MetaDataReader {

    protected String[] fieldNames;
    protected int[] fieldSizes;
    protected int totalLenght = 0;
    protected List<MetaData> metaData;

    public TarMetaDataReader() {
        fieldNames = new String[16];
        fieldSizes = new int[16];

        fieldNames[0] = "File name";
        fieldNames[1] = "File mode";
        fieldNames[2] = "Owner's numeric user ID";
        fieldNames[3] = "Group's numeric user ID";
        fieldNames[4] = "File size in bytes";
        fieldNames[5] = "Last modification time in numeric Unix time format";
        fieldNames[6] = "Checksum for header record";
        fieldNames[7] = "Link indicator";
        fieldNames[8] = "Name of linked file";
        fieldNames[9] = "magic";
        fieldNames[10] = "version";
        fieldNames[11] = "uname";
        fieldNames[12] = "gname";
        fieldNames[13] = "devmajor";
        fieldNames[14] = "devminor";
        fieldNames[15] = "prefix";

        fieldSizes[0] = 100;
        fieldSizes[1] = 8;
        fieldSizes[2] = 8;
        fieldSizes[3] = 8;
        fieldSizes[4] = 12;
        fieldSizes[5] = 12;
        fieldSizes[6] = 8;
        fieldSizes[7] = 1;
        fieldSizes[8] = 100;
        fieldSizes[9] = 6;
        fieldSizes[10] = 2;
        fieldSizes[11] = 32;
        fieldSizes[12] = 32;
        fieldSizes[13] = 8;
        fieldSizes[14] = 8;
        fieldSizes[15] = 155;

        for (int i : fieldSizes) {
            totalLenght += i;
        }
        metaData = new LinkedList<MetaData>();
    }

    @Override
    public List<MetaData> getMetaData(String filePath) throws WrongFormatException {

        byte[] data = new byte[totalLenght];
        checkFileName(filePath);
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath))) {
            in.read(data, 0, totalLenght);
            ByteArrayInputStream byteStream = new ByteArrayInputStream(data);
            int i = 0;
            for (int s : fieldSizes) {
                byte[] tmp = new byte[s];
                byteStream.read(tmp, 0, s);
                int trimSize = 0;
                for (byte ch : tmp) {
                    if (ch != 0) {
                        trimSize++;
                    }
                }
                byte[] trimmed = new byte[trimSize];
                int j = 0;
                for (byte ch : tmp) {
                    if (ch != 0) {
                        trimmed[j] = ch;
                        j++;
                    }
                }
                metaData.add(new MetaData(fieldNames[i], new String(trimmed, "UTF-8")));
                i++;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return metaData;
    }

    protected boolean checkFileName(String name) throws WrongFormatException {
        if(!name.matches(".*\\.tar")) throw new WrongFormatException();
        return true;
    }
}