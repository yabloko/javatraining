package com.training.metadatareader;

/**
 * Created by andrew on 18.12.14.
 */
public class MetaData {
    protected String fieldsName;
    protected String fieldValue;

    public MetaData(String name, String value) {
        fieldValue = value;
        fieldsName = name;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public String getFieldName() { return fieldsName; }
}
