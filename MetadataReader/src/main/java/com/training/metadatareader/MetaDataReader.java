package com.training.metadatareader;

import java.util.List;

/**
 * Created by andrew on 18.12.14.
 */
public interface MetaDataReader {
    public List<MetaData> getMetaData(String path) throws WrongFormatException;
}
