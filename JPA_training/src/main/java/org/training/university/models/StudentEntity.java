package org.training.university.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by andrew on 19.02.15.
 */
@Entity
@Table(name = "student", schema = "public")
public class StudentEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Basic
    @Column(name = "firstname")
    private String firstname;
    @Basic
    @Column(name = "lastname")
    private String lastname;
    @Basic
    @Column(name = "datecreated", insertable = false)
    private Timestamp datecreated;
    @Basic
    @Column(name = "dateedited", insertable = false)
    private Timestamp dateedited;

    @JoinColumn(name = "groupId")
    @ManyToOne
    private GroupEntity groupId;

    public GroupEntity getGroupId() {
        return groupId;
    }

    public void setGroupId(GroupEntity groupId) {
        this.groupId = groupId;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Timestamp getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Timestamp datecreated) {
        this.datecreated = datecreated;
    }

    public Timestamp getDateedited() {
        return dateedited;
    }

    public void setDateedited(Timestamp dateedited) {
        this.dateedited = dateedited;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentEntity that = (StudentEntity) o;

        if (datecreated != null ? !datecreated.equals(that.datecreated) : that.datecreated != null) return false;
        if (dateedited != null ? !dateedited.equals(that.dateedited) : that.dateedited != null) return false;
        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (lastname != null ? !lastname.equals(that.lastname) : that.lastname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (datecreated != null ? datecreated.hashCode() : 0);
        result = 31 * result + (dateedited != null ? dateedited.hashCode() : 0);
        return result;
    }
}
