package org.training.university.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by andrew on 19.02.15.
 */
@Entity
@Table(name = "faculty", schema = "public")

public class FacultyEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "datecreated", insertable = false)
    private Timestamp datecreated;
    @Basic
    @Column(name = "dateedited", insertable = false)
    private Timestamp dateedited;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Timestamp datecreated) {
        this.datecreated = datecreated;
    }

    public Timestamp getDateedited() {
        return dateedited;
    }

    public void setDateedited(Timestamp dateedited) {
        this.dateedited = dateedited;
    }
}
