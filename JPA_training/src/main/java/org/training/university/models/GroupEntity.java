package org.training.university.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by andrew on 19.02.15.
 */
@Entity
@Table(name = "group", schema = "public")
@SequenceGenerator(
        name="groupSerialGenerator",
        sequenceName="group_id_seq",
        allocationSize = 1
)
public class GroupEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "datecreated", insertable = false)
    private Timestamp datecreated;

    @Basic
    @Column(name = "dateedited", insertable = false)
    private Timestamp dateedited;

    @JoinColumn(name = "facultyId")
    @ManyToOne
    private FacultyEntity facultyId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Timestamp datecreated) {
        this.datecreated = datecreated;
    }

    public Timestamp getDateedited() {
        return dateedited;
    }

    public void setDateedited(Timestamp dateedited) {
        this.dateedited = dateedited;
    }

    public FacultyEntity getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(FacultyEntity facultyId) {
        this.facultyId = facultyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupEntity that = (GroupEntity) o;

        if (datecreated != null ? !datecreated.equals(that.datecreated) : that.datecreated != null) return false;
        if (dateedited != null ? !dateedited.equals(that.dateedited) : that.dateedited != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (datecreated != null ? datecreated.hashCode() : 0);
        result = 31 * result + (dateedited != null ? dateedited.hashCode() : 0);
        return result;
    }
}
