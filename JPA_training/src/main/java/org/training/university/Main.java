package org.training.university;

import org.training.university.managers.FacultyManager;
import org.training.university.managers.UniversityEntityManager;
import org.training.university.models.FacultyEntity;

/**
 * Created by andrew on 17.02.15.
 */
public class Main {
    public static void main(String[] args) {
        try(UniversityEntityManager fManager = new UniversityEntityManager<FacultyEntity>()) {
            FacultyEntity faculty = new FacultyEntity();
            faculty.setName("faculty_1");
            fManager.create(faculty);
            System.out.println(faculty.getId());
            FacultyEntity fac = (FacultyEntity)fManager.read(faculty.getId());
            System.out.println(fac.getName());
            faculty.setName("faculty_2");
            fManager.update(faculty);
            fac = (FacultyEntity) fManager.read(faculty.getId());
            System.out.println(fac.getName());
            fManager.delete(faculty);
            //System.out.println(fManager.read(faculty.getId()).getName());
        }
        //TODO clarify exception types
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
