package org.training.university.interfaces;

/**
 * Created by andrew on 19.02.15.
 */
public interface TEntityManager<T> extends AutoCloseable {
    public T create(T entity) throws Exception;
    public T read(Integer id) throws Exception;
    public T update(T entity) throws Exception;
    public void delete(T entity) throws Exception;
    public void close() throws Exception;
}
