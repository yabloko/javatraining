package org.training.university.managers;

import org.training.university.interfaces.TEntityManager;
import org.training.university.models.FacultyEntity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by andrew on 19.02.15.
 */
public class FacultyManager implements TEntityManager<FacultyEntity> {

    private EntityManager eManager = null;

    public FacultyManager() {
        eManager = Persistence.createEntityManagerFactory("NewPersistenceUnit").createEntityManager();
    }

    @Override
    public FacultyEntity create(FacultyEntity entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.persist(entity);
        eManager.getTransaction().commit();
        return entity;
    }

    @Override
    public FacultyEntity read(Integer id) throws Exception {
        return eManager.find(FacultyEntity.class, id);
    }

    @Override
    public FacultyEntity update(FacultyEntity entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.merge(entity);
        eManager.getTransaction().commit();
        return entity;
    }

    @Override
    public void delete(FacultyEntity entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.remove(entity);
        eManager.getTransaction().commit();
    }

    @Override
    public void close() throws Exception {
        eManager.close();
    }

}
