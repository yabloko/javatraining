package org.training.university.managers;

import org.training.university.models.FacultyEntity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.lang.reflect.ParameterizedType;

/**
 * Created by andrew on 05.03.15.
 */
public class UniversityEntityManager<T> implements AutoCloseable {
    private EntityManager eManager = null;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public UniversityEntityManager() {
        entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).
                getActualTypeArguments()[0];
        eManager = Persistence.createEntityManagerFactory("NewPersistenceUnit").createEntityManager();
    }

    public T create(T entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.persist(entity);
        eManager.getTransaction().commit();
        return entity;
    }

    public T read(Integer id) throws Exception {
        return eManager.find(entityClass, id);
    }

    public T update(T entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.merge(entity);
        eManager.getTransaction().commit();
        return entity;
    }

    public void delete(T entity) throws Exception {
        eManager.getTransaction().begin();
        eManager.remove(entity);
        eManager.getTransaction().commit();
    }

    public void close() throws Exception {
        eManager.close();
    }

}
