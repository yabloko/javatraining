package com.training.multithreadbenckmark;

/**
 * Created by andrew on 13.01.15.
 */
public interface ArraySorter {
    public void parallelRun();
}
