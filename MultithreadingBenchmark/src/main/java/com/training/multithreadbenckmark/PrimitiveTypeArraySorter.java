package com.training.multithreadbenckmark;


import java.util.Arrays;
import java.util.Random;

/**
 * Created by andrew on 09.01.15.
 */
public class PrimitiveTypeArraySorter implements ArraySorter {

    protected int[] array;

    public void parallelRun() {
        Arrays.parallelSort(Arrays.copyOf(array, array.length));
    }

    public PrimitiveTypeArraySorter(int size) {
        array = new int[size];
        Random rand = new Random();
        for(int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt();
        }
    }
}
