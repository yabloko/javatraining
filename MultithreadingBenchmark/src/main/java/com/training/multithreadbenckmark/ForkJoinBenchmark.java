package com.training.multithreadbenckmark;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by andrew on 10.01.15.
 */
public class ForkJoinBenchmark {

    public static void main(String[] args) {

        int arrayInitialSize = 10;
        int arraySizeMultiplier = 10;
        int arrayMaxSize = 100000;
        double[] results = new double[5];
        long execTime = 0;
        int iterations = 10;
        int j = 0;

        //warmup
        ArraySorter sorter = null;
        //sorter = new PrimitiveTypeArraySorter(1000000);
        sorter = new ObjectArraySorter(100000);
        sorter.parallelRun();
        //benchmark
        for (int i = arrayInitialSize; i <= arrayMaxSize; i = i * arraySizeMultiplier) {
            execTime = System.nanoTime();
            sorter = new ObjectArraySorter(i);
            for(int k = 0; k < iterations; k++) {
                sorter.parallelRun();
            }
            results[j] = Math.log10(System.nanoTime() - execTime);
            j++;
        }
        putToFile(results);
        System.out.println("completed");
    }

    protected static void putToFile(double[] out) {
        try {
            PrintWriter pWriter = new PrintWriter("/home/andrew/benchmark/" + ForkJoinPool.commonPool().getParallelism() + "_treadRun.txt");
            for (double item : out) {
                pWriter.println(item);
            }
            pWriter.close();
        } catch (FileNotFoundException e) {
            System.out.print(e.getMessage());
        }
    }
}
