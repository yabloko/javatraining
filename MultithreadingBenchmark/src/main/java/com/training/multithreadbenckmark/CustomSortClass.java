package com.training.multithreadbenckmark;

import java.util.Comparator;

/**
 * Created by andrew on 13.01.15.
 */
public class CustomSortClass implements Comparable {
    protected int value;
    /**
     * Constructs a newly allocated {@code Integer} object that
     * represents the specified {@code int} value.
     *
     * @param val the value to be represented by the
     *              {@code Integer} object.
     */
    public CustomSortClass(int val) {
        value = val;
    }

    public int intValue(){
        return value;
    }

    public int compareTo(Object o) {
          return Integer.valueOf(this.intValue()).compareTo(Integer.valueOf(((CustomSortClass) o).intValue()));
//        if(this.intValue() < a.intValue()) return -1;
//        if(this.intValue() > a.intValue()) return 1;
//        return 0;
    }
}
