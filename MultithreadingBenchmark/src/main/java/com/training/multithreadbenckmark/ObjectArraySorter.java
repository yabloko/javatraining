package com.training.multithreadbenckmark;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by andrew on 13.01.15.
 */
public class ObjectArraySorter implements ArraySorter
{
    protected CustomSortClass[] array;

    public void parallelRun() {
        Arrays.parallelSort(Arrays.copyOf(array, array.length));
    }

    public ObjectArraySorter(int size) {
        array = new CustomSortClass[size];
        Random rand = new Random();
        for(int i = 0; i < array.length; i++) {
            array[i] = new CustomSortClass(rand.nextInt());
        }
    }
}
