package com.training.BufferedBenchmark;

import java.io.*;
import java.util.Random;

/**
 * Created by andrew on 12.12.14.
 */
public class Benchmark {

    public static void main(String args[]) {
        int size = 4096 * 32;
        long execTime = 0;
        byte[] buf = new byte[size];
        byte[] out = new byte[size];
        int testRuns = 20;
        long totalNonBuffered = 0;
        float avgNonBuffered = 0;
        long totalBuffered = 0;
        float avgBuffered = 0;

        InputStream in = null;

       /* new Random().nextBytes(buf);
        in = new ByteArrayInputStream(buf);
*/

        try {
            in = new FileInputStream("/home/andrew/tmp");
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }

        for (int i = 0; i < size; i++) {
            try {
                out[i] = (byte) in.read();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        try {
            in.close();
            in = new FileInputStream("/home/andrew/tmp");
            //in = new ByteArrayInputStream(buf);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }

        for (int j = 0; j < testRuns; j++) {
            out = new byte[size];
            execTime = System.nanoTime();
            for (int i = 0; i < size; i++) {
                try {
                    out[i] = (byte) in.read();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            totalNonBuffered += System.nanoTime() - execTime;
            System.out.println("nonbuffered time = " + String.format("%d", System.nanoTime() - execTime));
            try {
                in.close();
                in = new FileInputStream("/home/andrew/tmp");
                //in = new ByteArrayInputStream(buf);
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
            }
        }
        avgNonBuffered = (float)totalNonBuffered/(float)testRuns;

        InputStream buffIn = new BufferedInputStream(in);
        buffIn.mark(size);
        for (int i = 0; i < size; i++) {
            try {
                out[i] = (byte) buffIn.read();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        try {
            buffIn.reset();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        for (int j = 0; j < testRuns; j++) {
            out = new byte[size];
            execTime = System.nanoTime();
            for (int i = 0; i < size; i++) {
                try {
                    out[i] = (byte) buffIn.read();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            totalBuffered += System.nanoTime() - execTime;
            System.out.println("buffered time = " + String.format("%d", System.nanoTime() - execTime));
            try {
                buffIn.reset();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        avgBuffered = (float)totalBuffered/(float)testRuns;

        System.out.println("buffered is "  + String.format("%3f", avgNonBuffered/avgBuffered) + "times faster");
    }
}
