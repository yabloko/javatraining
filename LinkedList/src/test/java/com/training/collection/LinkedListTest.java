package com.training.collection;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class LinkedListTest {

    private LinkedList<String> list;

    @Before
    public void initTest() {
        list = new LinkedList<String>();
        list.add("first");
        list.add("second");
        list.add("third");
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(list.size(), 3);
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertFalse(list.isEmpty());
    }

    @Test
    public void testIsEmptyEmptyList() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        assertTrue(emptyList.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        assertTrue(list.contains("second"));
        assertFalse(list.contains("random"));
        assertFalse(list.contains(new Integer(0)));
        LinkedList<String> emptyList = new LinkedList<String>();
        assertFalse(emptyList.contains("random"));
    }

    @Test
    public void testContainsEmptyList() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        assertFalse(emptyList.contains("random"));
    }

    @Test
    public void testIterator() throws Exception {
        list.add("fourth");
        list.add("fifth");
        list.add("sixth");
        String[] compareTo = new String[6];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        compareTo[3] = "fourth";
        compareTo[4] = "fifth";
        compareTo[5] = "sixth";
        int i = 0;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
    }

    @Test
    public void testIteratorForeach() throws Exception {
        list.add("fourth");
        list.add("fifth");
        list.add("sixth");
        String[] compareTo = new String[6];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        compareTo[3] = "fourth";
        compareTo[4] = "fifth";
        compareTo[5] = "sixth";
        int i = 0;
        Iterator<String> iterator = list.iterator();
        for (String item : list) {
            assertEquals(compareTo[i], item);
            i++;
        }
    }

    @Test
    public void testToArray() throws Exception {
        LinkedList<String> empty = new LinkedList<String>();
        assertEquals(empty.toArray().length, 0);
        assertEquals(list.toArray().length, 3);
    }

    @Test
    public void testToArrayWithParam() throws Exception {
        assertEquals(list.toArray(new String[3]).length, 3);
        String[] compareTo = new String[3];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        int i = 0;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 2);
    }

    @Test
    public void testToArrayWithParamEmptyList() throws Exception {
        LinkedList<String> empty = new LinkedList<String>();
        assertEquals(empty.toArray(new String[0]).length, 0);
    }

    @Test
    public void testToArrayWithParamZeroLengthArray() throws Exception {
        assertEquals(list.toArray(new String[0]).length, 3);
        String[] compareTo = new String[3];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        int i = 0;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 2);
    }

    @Test
    public void testToArrayWithParamArraySizeLessThanListSize() throws Exception {
        assertEquals(list.toArray(new String[2]).length, 3);
        String[] compareTo = new String[3];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        int i = 0;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 2);
    }

    @Test
    public void testToArrayWithParamArraySizeGreaterThanListSize() throws Exception {
        assertEquals(list.toArray(new String[4]).length, 4);
        String[] compareTo = new String[4];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        compareTo[3] = null;
        int i = 0;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 2);
    }

    @Test(expected = NullPointerException.class)
    public void testToArrayWithParamNullPointerException() throws Exception {
        list.toArray(null);
    }

    @Test
    public void testAdd() throws Exception {
        list.add("fourth");
        assertTrue(list.contains("fourth"));
        assertEquals(list.indexOf("fourth"), 3);
        assertEquals(list.size(), 4);
    }

    @Test
    public void testAddWithIndexCommonElement() throws Exception {
        list.add(2, "random");
        assertEquals(list.indexOf("random"), 2);
        assertEquals(list.lastIndexOf("third"), 3);
        assertEquals(list.size(), 4);
    }

    @Test
    public void testAddWithIndexFirstElement() throws Exception {
        list.add(0, "zero");
        assertEquals(list.indexOf("zero"), 0);
        assertEquals(list.size(), 4);
    }

    @Test
    public void testAddWithIndexLastElement() throws Exception {
        list.add(3, "last");
        assertEquals(list.indexOf("last"), 3);
        assertEquals(list.size(), 4);
    }

    @Test
    public void testAddWithIndexOnlyElement() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        emptyList.add(0, "first");
        assertEquals(emptyList.indexOf("first"), 0);
        assertEquals(emptyList.size(), 1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddWithIndexIndexOutOfBoundsException() throws Exception {
        list.add(4, "random");
    }

    @Test
    public void testSetCommonElement() throws Exception {
        assertEquals(list.set(1, "random"), "random");
        assertEquals(list.indexOf("random"), 1);
        assertEquals(list.size(), 3);
    }

    @Test
    public void testSetFirstElement() throws Exception {
        assertEquals(list.set(0, "zero"), "zero");
        assertEquals(list.indexOf("zero"), 0);
        assertEquals(list.indexOf("third"), 2);
        assertEquals(list.size(), 3);
    }

    @Test
    public void testSetLastElement() throws Exception {
        assertEquals(list.set(2, "last"), "last");
        assertEquals(list.indexOf("last"), 2);
        assertEquals(list.size(), 3);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexOutOfBoundsException() throws Exception {
        list.set(3, "random");
    }

    @Test
    public void testRemoveNonExisting() throws Exception {
        assertFalse(list.remove("random"));
    }

    @Test
    public void testRemoveCommonElement() throws Exception {
        //remove common element
        assertTrue(list.remove("second"));
        assertFalse(list.contains("second"));
        assertEquals(list.size(), 2);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveCommonElementDuplicate() throws Exception {
        //remove common element
        list.add("second");
        assertTrue(list.remove("second"));
        assertEquals(list.indexOf("second"), 2);
        assertEquals(list.size(), 3);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveFirstElement() throws Exception {
        assertTrue(list.remove("first"));
        assertFalse(list.contains("first"));
        assertEquals(list.size(), 2);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveLastElement() throws Exception {
        assertTrue(list.remove("third"));
        assertFalse(list.contains("third"));
        assertEquals(list.size(), 2);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveOnlyElement() throws Exception {
        LinkedList<String> oneElementList = new LinkedList<String>();
        oneElementList.add("only");
        assertTrue(oneElementList.remove("only"));
        assertEquals(oneElementList.size(), 0);
        assertTrue(oneElementList.isEmpty());
    }

    @Test
    public void testContainsAll() throws Exception {
        ArrayList<String> query = new ArrayList<String>(2);
        query.add(0, "second");
        query.add(1, "first");
        assertTrue(list.containsAll(query));
    }

    @Test
    public void testContainsAllEmptyList() throws Exception {
        ArrayList<String> emptyArray = new ArrayList<String>();
        LinkedList<String> emptyList = new LinkedList<String>();
        assertTrue(emptyList.containsAll(emptyArray));
    }

    @Test
    public void testContainsAllSelfReference() throws Exception {
        assertTrue(list.containsAll(list));
    }

    @Test
    public void testAddAll() throws Exception {
        ArrayList<String> empty = new ArrayList<String>(0);
        assertFalse(list.addAll(empty));

        ArrayList<String> reference = new ArrayList<String>(6);

        reference.add("first");
        reference.add("second");
        reference.add("third");
        reference.add("fourth");
        reference.add("fifth");
        reference.add("sixth");

        ArrayList<String> toAdd = new ArrayList<String>(3);
        toAdd.add("fourth");
        toAdd.add("fifth");
        toAdd.add("sixth");
        assertTrue(list.addAll(toAdd));
        assertEquals(6, list.size());
        ListIterator<String> iterator = list.listIterator();
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), reference.get(i));
            i++;
        }
        while (iterator.hasPrevious()) {
            assertEquals(iterator.previous(), reference.get(i));
            i--;
        }
    }

    @Test
    public void testAddAllWithIndex() throws Exception {
        ArrayList<String> reference = new ArrayList<String>(6);

        reference.add("first");
        reference.add("fourth");
        reference.add("fifth");
        reference.add("sixth");
        reference.add("second");
        reference.add("third");

        ArrayList<String> toAdd = new ArrayList<String>(3);
        toAdd.add("fourth");
        toAdd.add("fifth");
        toAdd.add("sixth");
        assertTrue(list.addAll(1, toAdd));
        assertEquals(6, list.size());
        ListIterator<String> iterator = list.listIterator();
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), reference.get(i));
            i++;
        }
        while (iterator.hasPrevious()) {
            assertEquals(iterator.previous(), reference.get(i));
            i--;
        }
    }

    @Test
    public void testAddAllWithFirstIndex() throws Exception {
        ArrayList<String> reference = new ArrayList<String>(6);

        reference.add("fourth");
        reference.add("fifth");
        reference.add("sixth");
        reference.add("first");
        reference.add("second");
        reference.add("third");

        ArrayList<String> toAdd = new ArrayList<String>(3);
        toAdd.add("fourth");
        toAdd.add("fifth");
        toAdd.add("sixth");
        assertTrue(list.addAll(0, toAdd));
        assertEquals(6, list.size());
        ListIterator<String> iterator = list.listIterator();
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), reference.get(i));
            i++;
        }
        while (iterator.hasPrevious()) {
            assertEquals(iterator.previous(), reference.get(i));
            i--;
        }
    }

    @Test
    public void testAddAllWithLastIndex() throws Exception {
        ArrayList<String> reference = new ArrayList<String>(6);

        reference.add("first");
        reference.add("second");
        reference.add("third");
        reference.add("fourth");
        reference.add("fifth");
        reference.add("sixth");

        ArrayList<String> toAdd = new ArrayList<String>(3);
        toAdd.add("first");
        toAdd.add("second");
        toAdd.add("third");
        assertTrue(list.addAll(3, toAdd));
        assertEquals(6, list.size());
        ListIterator<String> iterator = list.listIterator();
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), reference.get(i));
            i++;
        }
        while (iterator.hasPrevious()) {
            assertEquals(iterator.previous(), reference.get(i));
            i--;
        }
    }

    @Test
    public void testAddAllWithIndexEmptyList() throws Exception {
        ArrayList<String> empty = new ArrayList<String>(0);
        assertFalse(list.addAll(empty));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddAllWithIndexIndexOutOfBoundsException() throws Exception {
        list.addAll(4, new ArrayList<String>(0));
    }

    @Test
    public void testRemoveAllAllItems() throws Exception {
        ArrayList<String> toRemove = new ArrayList<String>();
        toRemove.add("third");
        toRemove.add("second");
        toRemove.add("first");
        assertTrue(list.removeAll(toRemove));
        assertEquals(list.size(), 0);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testRemoveAllSomeItems() throws Exception {
        ArrayList<String> toRemove = new ArrayList<String>();
        toRemove.add("third");
        toRemove.add("second");
        assertTrue(list.removeAll(toRemove));
        assertTrue(list.contains("first"));
        assertEquals(list.size(), 1);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveAllEmptyList() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        ArrayList<String> toRemove = new ArrayList<String>();
        toRemove.add("third");
        toRemove.add("second");
        assertFalse(emptyList.removeAll(toRemove));
        assertEquals(emptyList.size(), 0);
        assertTrue(emptyList.isEmpty());
    }

    @Test
    public void testRemoveAllEmptyQuery() throws Exception {
        ArrayList<String> toRemove = new ArrayList<String>();
        assertFalse(list.removeAll(toRemove));
        assertEquals(list.size(), 3);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRemoveAllEmptyListAndEmptyQuery() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        ArrayList<String> toRemove = new ArrayList<String>();
        assertFalse(emptyList.removeAll(toRemove));
        assertEquals(emptyList.size(), 0);
        assertTrue(emptyList.isEmpty());
    }

    @Test
    public void testRetainAllAllItems() throws Exception {
        ArrayList<String> toRetain = new ArrayList<String>();
        toRetain.add("third");
        toRetain.add("second");
        toRetain.add("first");
        assertFalse(list.retainAll(toRetain));
        assertEquals(list.size(), 3);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRetainAllSomeItems() throws Exception {
        ArrayList<String> toRetain = new ArrayList<String>();
        toRetain.add("third");
        toRetain.add("second");
        assertTrue(list.retainAll(toRetain));
        assertTrue(list.contains("second"));
        assertTrue(list.contains("third"));
        assertEquals(list.size(), 2);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testRetainAllEmptyList() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        ArrayList<String> toRetain = new ArrayList<String>();
        toRetain.add("third");
        toRetain.add("second");
        assertFalse(emptyList.retainAll(toRetain));
        assertEquals(emptyList.size(), 0);
        assertTrue(emptyList.isEmpty());
    }

    @Test
    public void testRetainAllEmptyQuery() throws Exception {
        ArrayList<String> toRetain = new ArrayList<String>();
        assertTrue(list.retainAll(toRetain));
        assertEquals(list.size(), 0);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testRetainAllEmptyListAndEmptyQuery() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        ArrayList<String> toRetain = new ArrayList<String>();
        assertFalse(emptyList.retainAll(toRetain));
        assertEquals(emptyList.size(), 0);
        assertTrue(emptyList.isEmpty());
    }

    @Test
    public void testClear() throws Exception {
        list.clear();
        assertTrue(list.isEmpty());
        assertEquals(list.size(), 0);
        LinkedList<String> emptyList = new LinkedList<String>();
        emptyList.clear();
        assertTrue(emptyList.isEmpty());
        assertEquals(emptyList.size(), 0);

    }

    @Test
    public void testGet() throws Exception {
        assertEquals(list.get(1), "second");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexOutOfBoundsException() throws Exception {
        list.get(3);
    }

    @Test
    public void testIndexOf() throws Exception {
        list.add("third");
        assertEquals(list.indexOf("third"), 2);
        assertEquals(list.indexOf("random"), -1);
    }

    @Test
    public void testLastIndexOf() throws Exception {
        list.add("first");
        assertEquals(list.lastIndexOf("first"), 3);
        assertEquals(list.lastIndexOf("random"), -1);
    }

    @Test
    public void testListIterator() throws Exception {
        list.add("fourth");
        list.add("fifth");
        list.add("sixth");
        ListIterator<String> iterator = list.listIterator();
        String[] compareTo = new String[6];
        compareTo[0] = "first";
        compareTo[1] = "second";
        compareTo[2] = "third";
        compareTo[3] = "fourth";
        compareTo[4] = "fifth";
        compareTo[5] = "sixth";
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 5);
        while (iterator.hasPrevious()) {
            assertEquals(iterator.previous(), compareTo[i]);
            i--;
        }
        assertEquals(i, 0);
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
        assertEquals(i, 5);
    }

    @Test
    public void testListIteratorNextIndex() throws Exception {
        ListIterator<String> iterator = list.listIterator();

        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(i + 1, iterator.nextIndex());
            iterator.next();
            assertEquals(i + 2, iterator.nextIndex());
            i++;
        }
    }

    @Test
    public void testListIteratorPreviousIndex() throws Exception {
        ListIterator<String> iterator = list.listIterator();

        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(i - 1, iterator.previousIndex());
            iterator.next();
            assertEquals(i, iterator.previousIndex());
            i++;
        }
    }

    @Test
    public void testListIteratorWithIndex() throws Exception {
        assertTrue(true);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListIteratorWithIndexIndexOutOfBoundsException() throws Exception {
        list.listIterator(3);
    }

    @Test
    public void testSubList() throws Exception {
        list.add("fourth");
        list.add("fifth");
        list.add("sixth");
        LinkedList<String> subList = list.subList(1, 3);
        ListIterator<String> iterator = subList.listIterator();
        String[] compareTo = new String[3];

        compareTo[0] = "second";
        compareTo[1] = "third";
        compareTo[2] = "fourth";
;
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(compareTo[i], iterator.next());
            i++;
        }
    }

    @Test
    public void testSubListZeroLength() throws Exception {
        LinkedList<String> subList = list.subList(1, 1);
        assertTrue(subList.isEmpty());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSubListIndexOutOfBoundsExceptionInvalidRange() throws Exception {
        list.subList(1, 0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSubListIndexOutOfBoundsExceptionToOutOfBounds() throws Exception {
        list.subList(1, 3);
    }
}