package com.training.collection;

import java.lang.reflect.Array;
import java.util.*;
import java.util.LinkedList
/**
 * Created by andrew on 14.11.14.
 * @version 0.1
 * a doubly linked list custom implementation
 * for educational properties
 * @param <T> type of object to be stored in list
 * @author andrew
 * @since 0.1
 */

public final class LinkedList<T> implements List<T>/*,Deque<T>*/ {

    private int size = 0;
    private ListItem<T> first = null;
    private ListItem<T> last = null;

    /**
     * size of current array
     * @return number of elements in this list
     */
    public int size() {
        return size;
    }

    /**
     * find if current list is empty
     * @return true if this list contains no elements, e.g. size == 0
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * constructor without params
     */
    public LinkedList(){

    }

    /**
     * constructor with param, create new LinkedList based onexistsing Collection
     * @param c elements of this collection will be added to new list
     */
    public LinkedList(Collection<? extends T> c) {
        this();
        addAll(c);
    }

    /**
     * find if current list contains element
     * @param o element to seek in list
     * @return true if list contains at least one element such that o.equals(element) == true
     */
    public boolean contains(Object o) {
        if (!isEmpty()) {
            ListItem<T> fromStart = first;
            ListItem<T> fromEnd = last;
            //copied from java.util.LinkedList and save millions of bytes of memory!!!!111
            int half = (size >> 1) + 1;
            int i = 0;
            while (i < half) {
                if (fromStart.value.equals(o) || fromEnd.value.equals(o)) {
                    return true;
                }
                fromStart = fromStart.next;
                fromEnd = fromEnd.previous;
                i++;
            }
            return false;
        }
        return false;
    }

    /**
     * get iterator over current list
     * @return iterator object implements Iterator
     */
    public Iterator<T> iterator() {
        return iterator(0);
    }

    /**
     * cast current list to array of Objects
     * containing elements of this list in proper sequence
     * @return array of Object elements
     */
    public Object[] toArray() {
        if (!isEmpty()) {
            Object[] a = new Object[size];
            ListItem<T> current = first;
            int i = 0;
            while (current != null) {
                a[i] = current.value;
                current = current.next;
                i++;
            }
            return a;
        }
        return new Object[0];
    }

    /**
     * cast current list to array of T elements
     * @param a array to store elements in, if not big enough, new array will be allocated
     * @param <E> extends T, or T could be safely cast into E
     * @return array of E elements containing elements of this list in proper sequence
     * @throws ArrayStoreException actually never thrown
     * @throws NullPointerException if a is null
     */
    public <E> E[] toArray(E[] a) throws ArrayStoreException, NullPointerException {
        if (a == null) throw new NullPointerException();
        if (!isEmpty()) {
            if (a.length <= size) a = (E[]) Array.newInstance(a.getClass().getComponentType(), size);
            ListItem<T> current = first;
            int i = 0;
            while (current != null) {
                a[i] = (E) current.value;
                current = current.next;
                i++;
            }
            if (i < a.length - 1) {
                while (i < a.length) {
                    a[i] = null;
                    i++;
                }

            }
            return a;
        }
        return a;
    }

    /**
     * add element to current list
     * @param e object to add to array
     * @return true on success
     */
    public boolean add(T e) {
        ListItem<T> item = new ListItem<T>(e);
        if (isEmpty()) {
            first = last = item;
        } else {
            item.previous = last;
            last.next = item;
            last = item;
        }
        size++;
        return true;
    }

    /**
     * remove first occurrence of o in list
     * @param o object to seek to in current list
     * @return true if object was removed (e.g. list modified)
     */
    public boolean remove(Object o) {
        if (!isEmpty()) {
            ListItem<T> current = first;

            while (current != null) {
                if (current.value.equals(o)) {
                    if (removeItem(current)) return true;
                }
                current = current.next;
            }
        }
        return false;
    }

    /**
     * find if current list contains all elements of collection c
     * @param c collection, elements of which to seek in current list
     * @return true if list contains all elements of c, false if not
     */
    public boolean containsAll(Collection<?> c) {
        if (isEmpty() && c.isEmpty()) return true;
        if (!isEmpty() && size >= c.size()) {
            Iterator<?> iterator = c.iterator();
            while (iterator.hasNext()) {
                if (!contains(iterator.next())) return false;
            }
            return true;
        }
        return false;
    }

    /**
     * add all elements of collection to current list
     * @param c collection, elements of which to add
     * @return true if current list was modified
     */
    public boolean addAll(Collection<? extends T> c) {
        if (c.size() > 0) {
            Iterator<? extends T> iterator = c.iterator();
            while (iterator.hasNext()) {
                add(iterator.next());
            }
            return true;
        }
        return false;
    }

    /**
     * add all elements of collection c to current list, starting from index position
     * index =&gt;0 &amp;&amp; index &lt; size()
     * @param index index to start insertion of new elements
     * @param c collection, which elements to add to current list
     * @return true if current list was modified
     * @throws IndexOutOfBoundsException if index &lt; 0 || index &gt;= size()
     */
    public boolean addAll(int index, Collection<? extends T> c) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();

        if (c.size() > 0) {
            ListItem<T> rightItem = getItem(index);
            ListItem<T> leftItem = rightItem != null ? rightItem.previous : null;
            LinkedList<T> toAdd = new LinkedList<T>();
            toAdd.addAll(c);
            //index was not == size
            if (rightItem != null) {
                toAdd.last.next = rightItem;
                rightItem.previous = toAdd.last;
            }
            //else - new pointer to end
            else {
                last = toAdd.last;
            }
            //index was not 0
            if (leftItem != null) {
                toAdd.first.previous = leftItem;
                leftItem.next = toAdd.first;
            }
            //else - new pointer to start
            else {
                first = toAdd.first;
            }
            size += toAdd.size;
            return true;
        }
        return false;
    }

    /**
     * remove all element of current array that are present in given collection c
     * @param c collection to compare elements of current list
     * @return true if current list was modified
     */
    public boolean removeAll(Collection<?> c) {
        if (c.isEmpty() || isEmpty()) return false;
        boolean modified = false;
        Iterator<?> iterator = c.iterator();
        ListItem<T> fromStart = first;
        ListItem<T> fromEnd = last;
        int half;
        int i;
        //worst case t = (this.size/2)*c.size
        while (iterator.hasNext()) {
            half = (size >> 1) + 1;
            i = 0;
            Object value = iterator.next();
            fromStart = first;
            fromEnd = last;
            while (i < half) {
                if (fromStart != null && fromStart.value.equals(value)) {
                    removeItem(fromStart);
                    modified = true;
                }
                if (fromEnd != null && fromEnd.value.equals(value)) {
                    removeItem(fromEnd);
                    modified = true;
                }
                fromEnd = (fromEnd != null) ? fromEnd.previous : null;
                fromStart = (fromEnd != null) ? fromStart.next : null;
                i++;
            }
        }
        return modified;
    }

    /**
     * leave only elements of this list that are present in given collection
     * @param c collection to compare to elements of this list
     * @return true if current list was modified
     */
    public boolean retainAll(Collection<?> c) {
        if (isEmpty()) return false;

        if (c.isEmpty()) {
            first = last = null;
            size = 0;
            return true;
        }

        boolean modified = false;
        ListItem<T> fromStart = first;
        ListItem<T> fromEnd = last;

        int half = (size >> 1) + 1;
        int i = 0;
        while (i < half) {
            if (fromStart != null && !c.contains(fromStart.value)) {
                removeItem(fromStart);
                modified = true;
            }
            if (fromEnd != null && !c.contains(fromEnd.value)) {
                removeItem(fromEnd);
                modified = true;
            }
            fromEnd = (fromEnd != null) ? fromEnd.previous : null;
            fromStart = (fromEnd != null) ? fromStart.next : null;
            i++;
        }
        return modified;
    }

    /**
     * remove all elements from current list
     * makes isEmpty() == true and size() == 0
     */
    @Override
    public void clear() {
        if (!isEmpty()) {
            ListItem<T> current = first;

            while (current.next != null) {
                current = current.next;
                current.previous = null;
            }
            size = 0;
            first = last = null;
        }
    }

    /**
     * get element of the list in specified position
     * index &gt;= 0 &amp;&amp; index &lt; size()
     * @param index index of element to get
     * @return T object at given position
     * @throws IndexOutOfBoundsException if index &lt; 0 or index &gt;= size()
     */
    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        if (index > -1 && index < size && !isEmpty()) {
            return getItem(index).value;
        } else throw new IndexOutOfBoundsException();
    }

    /**
     * set element with index some value. element should exist already, e.g.
     * index &gt;= 0 &amp;&amp; index &lt; size();
     * @param index index of element to set value to
     * @param element object of type T to set
     * @return T on success
     * @throws IndexOutOfBoundsException if index &lt; 0 || index &gt;= size()
     */
    @Override
    public T set(int index, T element) throws IndexOutOfBoundsException {
        if (index > -1 && index < size && !isEmpty()) {
            getItem(index).value = element;
            return element;
        } else throw new IndexOutOfBoundsException();
    }

    /**
     * add element with given index, element could not exist already, e.g.
     * index &gt;= 0 &amp;&amp; index &lt;= size();
     * @param index index of element to set value to
     * @param element object of type T to set
     * @throws IndexOutOfBoundsException if index &lt; 0 || index &gt;= size() + 1
     */
    @Override
    public void add(int index, T element) {
        if (index > -1 && index < size + 1) {
            //if only element or last element added
            if (isEmpty() || index == size) add(element);
            else {
                ListItem<T> toAdd = new ListItem<T>(element);
                ListItem<T> current = getItem(index);
                //if common element added
                if (current.previous != null) {
                    current.previous.next = toAdd;
                }
                toAdd.previous = current.previous;
                current.previous = toAdd;
                toAdd.next = current;
                //if first element added
                if (index == 0) first = toAdd;
                size++;
            }

        } else throw new IndexOutOfBoundsException();
    }

    /**
     * remove element by its index,
     * index &gt;= 0 &amp;&amp; index &lt; size()
     * @param index index of element to remove
     * @return T object on success
     * @throws IndexOutOfBoundsException id index &lt; 0 || index &gt;= size()
     */
    @Override
    public T remove(int index) throws IndexOutOfBoundsException{
        if (index > -1 && index < size && !isEmpty()) {
            ListItem<T> current = getItem(index);
            ListItem<T> prev = current.previous;
            ListItem<T> next = current.next;
            T value = null;
            //only element
            if (prev == null && next == null) {
                first = last = null;
                size = 0;
                value = current.value;
                current = null;

            }
            //common element in the middle
            if (prev != null && next != null) {
                prev.next = next;
                next.previous = prev;
                size--;
                value = current.value;
                current = null;
            }
            //first element
            if (next != null) {
                next.previous = null;
                first = next;
                size--;
                value = current.value;
                current = null;
            }
            //last element
            if (prev != null) {
                prev.next = null;
                last = prev;
                size--;
                value = current.value;
                current = null;
            }
            return value;
        } else throw new IndexOutOfBoundsException();
    }

    /**
     * return index of first occurrence of o
     * seeks o by o.equals(element) == true
     * @param o object to seek in current list
     * @return index of found element or -1 if object is not present in list
     */
    @Override
    public int indexOf(Object o) {
        if (!isEmpty()) {
            ListItem<T> fromStart = first;
            int i = 0;
            while (i < size) {
                if (fromStart.value.equals(o)) {
                    return i;
                }
                fromStart = fromStart.next;
                i++;
            }
        }
        return -1;
    }

    /**
     * return index of last occurrence of o
     * seeks o by o.equals(element) == true
     * @param o object to seek in current list
     * @return index of found element or -1 if object is not present in list
     */
    @Override
    public int lastIndexOf(Object o) {
        if (!isEmpty()) {
            ListItem<T> fromEnd = last;
            int i = size - 1;
            while (i > -1) {
                if (fromEnd.value.equals(o)) {
                    return i;
                }
                fromEnd = fromEnd.previous;
                i--;
            }
        }
        return -1;
    }

    /**
     * return ListIterator over element of this list
     * @return ListIterator instance
     */
    @Override
    public ListIterator<T> listIterator() {
        return iterator(0);
    }

    /**
     * return ListIterator over element of this list starting with index
     * index &gt;= 0 &amp;&amp; index &lt; size()
     * @param index index of element to start iterating over list
     * @return ListIterator instance
     * @throws IndexOutOfBoundsException if index &lt; 0 || index &gt;= size()
     */
    @Override
    public ListIterator<T> listIterator(int index) throws IndexOutOfBoundsException {
        return iterator(index);
    }

    /**
     * return subset of elements of this list
     * fromIndex &lt;= toIndex &amp;&amp; fromIndex &gt;= 0 &amp;&amp; fromIndex &lt; size() &amp;&amp; toIndex &gt;= 0 &amp;&amp; toIndex &lt; size()
     * NOTE! this method does not implement List interface suggested behaviour, operations on sublist
     * do not affect ancestor list in any way!
     * if fromIndex == toIndex empty list is returned
     * @param fromIndex index to start subset with
     * @param toIndex index to end subset with
     * @return new LinkedList&lt;T&gt;containing element of current list from fromIndex to toIndex in proper sequence
     * @throws IndexOutOfBoundsException
     * if fromIndex &gt;toIndex || toIndex &lt; 0 || toIndex &gt;= size() || fromIndex &lt; 0 || fromIndex &gt;= size()
     */
    @Override
    public LinkedList<T> subList(int fromIndex, int toIndex) throws IndexOutOfBoundsException {
        if(fromIndex > -1 && toIndex < size && fromIndex < toIndex + 1) {
            LinkedList<T> subList = new LinkedList<T>();

            if(fromIndex == toIndex) return subList;

            int i = toIndex - fromIndex;
            ListItem<T> item = getItem(fromIndex);

            while(i > 0) {
                subList.add(item.value);
                item = item.next;
                i--;
            }
            return subList;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * method to actually create ListIterator with given index
     * @param index index to start iteration over current list
     * @return LinkedListIterator implements ListIterator instance
     */
    protected ListIterator<T> iterator(int index) {
        return new LinkedListIterator(index);
    }

    /**
     * internal method to getList item by given index
     * @param index index of ListItem to get
     * @return ListItem object by given index or null if index does not exist
     */
    private ListItem<T> getItem(int index) {
        if (index < size && index > -1 && !isEmpty()) {
            ListItem<T> item = null;
            //copied from java.util.LinkedList and save millions of bytes of memory!!!!111
            //+1 for ceil()
            int half = (size >> 1) + 1;
            int i = 0;
            if (index < half) {
                item = first;
                while (i != index) {
                    item = item.next;
                    i++;
                }
            } else {
                i = size - 1;
                item = last;
                while (i != index) {
                    item = item.previous;
                    i--;
                }
            }
            return item;
        }
        return null;
    }

    /**
     * remove ListItem of current list
     * @param item should be element of current list e.g. next and previous points to elements of current list
     * @return
     */
    private boolean removeItem(ListItem<T> item) {
        ListItem<T> prev = item.previous;
        ListItem<T> next = item.next;
        boolean result = false;
        //only element
        if (prev == null && next == null) {
            first = last = null;
            size = 0;
            result = true;
        }
        //common element in the middle
        else if (prev != null && next != null) {
            prev.next = next;
            next.previous = prev;
            size--;
            result = true;
        }
        //first element
        else if (next != null) {
            next.previous = null;
            first = next;
            size--;
            result = true;
        }
        //last element
        else if (prev != null) {
            prev.next = null;
            last = prev;
            size--;
            result = true;
        }
        item.previous = null;
        item.next = null;
        return result;
    }

    /**
     * ListIterator for this LinkedList implementation
     * @author andrew
     * @since 0.1
     */
    private class LinkedListIterator implements ListIterator<T> {
        private ListItem<T> returned;
        private ListItem<T> next;
        private int nextIndex = 1;

        /**
         * constructor with param - index to start iteration with
         * @param start index to start iterating with over elements of current list
         */
        LinkedListIterator(int start) {
            if (start < 0 || start > size - 1) throw new IndexOutOfBoundsException();
            if (start == 0) {
                next = first;
            } else {
                int half = size >> 1;
                int i = 0;
                if (start < half) {
                    next = first;
                    while (i != start) {
                        next = next.next;
                        i++;
                    }
                } else {
                    next = last;
                    i = size;
                    while (i != start) {
                        next = next.previous;
                        i--;
                    }
                }
            }
            nextIndex = start + 1;
        }

        /**
         * find if iterator has next element to jump to
         * @return true if there is next element iterator could jump to
         */
        public boolean hasNext() {
            return next.next != null;
        }

        /**
         * Returns the next element in the list and advances the cursor position.
         * This method may be called repeatedly to iterate through the list,
         * or intermixed with calls to {@link #previous} to go back and forth.
         * (Note that alternating calls to {@code next} and {@code previous}
         * will return the same element repeatedly.)
         *
         * @return the next element in the list
         * @throws NoSuchElementException if the iteration has no next element
         */
        public T next() throws NoSuchElementException {
            if (next != null) {
                returned = next;
                //stop at last element, do not jump to end of list nul pointer
                if (next.next != null) next = next.next;
                nextIndex++;
                return returned.value;
            } else throw new NoSuchElementException();
        }

        /**
         * Returns {@code true} if this list iterator has more elements when
         * traversing the list in the reverse direction.  (In other words,
         * returns {@code true} if {@link #previous} would return an element
         * rather than throwing an exception.)
         *
         * @return {@code true} if the list iterator has more elements when
         * traversing the list in the reverse direction
         */
        @Override
        public boolean hasPrevious() {
            return next.previous != null;
        }

        /**
         * Returns the previous element in the list and moves the cursor
         * position backwards.  This method may be called repeatedly to
         * iterate through the list backwards, or intermixed with calls to
         * {@link #next} to go back and forth.  (Note that alternating calls
         * to {@code next} and {@code previous} will return the same
         * element repeatedly.)
         *
         * @return the previous element in the list
         * @throws java.util.NoSuchElementException if the iteration has no previous
         *                                          element
         */
        @Override
        public T previous() throws NoSuchElementException {
            if (next.previous != null) {
                returned = next;
                //stop at first element, do not jump to list start null pointer
                next = next.previous;
                nextIndex--;
                return returned.value;
            } else throw new NoSuchElementException();
        }

        /**
         * Returns the index of the element that would be returned by a
         * subsequent call to {@link #next}. (Returns list size if the list
         * iterator is at the end of the list.)
         *
         * @return the index of the element that would be returned by a
         * subsequent call to {@code next}, or list size if the list
         * iterator is at the end of the list
         */
        @Override
        public int nextIndex() {
            return nextIndex;
        }

        /**
         * Returns the index of the element that would be returned by a
         * subsequent call to {@link #previous}. (Returns -1 if the list
         * iterator is at the beginning of the list.)
         *
         * @return the index of the element that would be returned by a
         * subsequent call to {@code previous}, or -1 if the list
         * iterator is at the beginning of the list
         */
        @Override
        public int previousIndex() {
            return nextIndex - 2;
        }

        /**
         * Removes from the list the last element that was returned by {@link
         * #next} or {@link #previous} (optional operation).  This call can
         * only be made once per call to {@code next} or {@code previous}.
         * It can be made only if {@link #add} has not been
         * called after the last call to {@code next} or {@code previous}.
         *
         * @throws UnsupportedOperationException if the {@code remove}
         *                                       operation is not supported by this list iterator
         * @throws IllegalStateException         if neither {@code next} nor
         *                                       {@code previous} have been called, or {@code remove} or
         *                                       {@code add} have been called after the last call to
         *                                       {@code next} or {@code previous}
         */
        @Override
        public void remove() throws UnsupportedOperationException {
            //TODO: implement
            throw new UnsupportedOperationException("remove");
        }

        /**
         * Replaces the last element returned by {@link #next} or
         * {@link #previous} with the specified element (optional operation).
         * This call can be made only if neither {@link #remove} nor {@link
         * #add} have been called after the last call to {@code next} or
         * {@code previous}.
         *
         * @param t the element with which to replace the last element returned by
         *          {@code next} or {@code previous}
         * @throws UnsupportedOperationException if the {@code set} operation
         *                                       is not supported by this list iterator
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws IllegalArgumentException      if some aspect of the specified
         *                                       element prevents it from being added to this list
         * @throws IllegalStateException         if neither {@code next} nor
         *                                       {@code previous} have been called, or {@code remove} or
         *                                       {@code add} have been called after the last call to
         *                                       {@code next} or {@code previous}
         */
        @Override
        public void set(T t) {
            //TODO: implement
        }

        /**
         * Inserts the specified element into the list (optional operation).
         * The element is inserted immediately before the element that
         * would be returned by {@link #next}, if any, and after the element
         * that would be returned by {@link #previous}, if any.  (If the
         * list contains no elements, the new element becomes the sole element
         * on the list.)  The new element is inserted before the implicit
         * cursor: a subsequent call to {@code next} would be unaffected, and a
         * subsequent call to {@code previous} would return the new element.
         * (This call increases by one the value that would be returned by a
         * call to {@code nextIndex} or {@code previousIndex}.)
         *
         * @param t the element to insert
         * @throws UnsupportedOperationException if the {@code add} method is
         *                                       not supported by this list iterator
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws IllegalArgumentException      if some aspect of this element
         *                                       prevents it from being added to this list
         */
        @Override
        public void add(T t) {
            //TODO: implement
        }
    }

    /**
     * @author andrew
     * @since 0.1
     * @param <T> type of object stored in current list
     */
    private static class ListItem<T> {

        private T value;

        public ListItem<T> next;
        public ListItem<T> previous;

        /**
         * constructor with param
         * @param item T object to store as current list element value
         */
        public ListItem(T item) {
            next = null;
            previous = null;
            value = item;
        }

        public boolean equals(ListItem<T> item) {
            return this == item;
        }
    }
}
