package com.training.BufferedFileCopy;

import java.io.*;

/**
 * Created by andrew on 16.12.14.
 */
public class PredefinedBenchmark {
    public static void main(String[] args) {
        long execTime = 0;
        String inFile = "/home/andrew/dst2";
        String outFile = "/home/andrew/out";
        int sizeCount = 10;
        int iterations = 5;

        int[] buffSizes = new int[sizeCount];
        buffSizes[0] = 1024;
        buffSizes[1] = 1024 * 2;
        buffSizes[2] = 1024 * 4;
        buffSizes[3] = 1024 * 8;
        buffSizes[4] = 1024 * 16;
        buffSizes[5] = 1024 * 32;
        buffSizes[6] = 1024 * 64;
        buffSizes[7] = 1024 * 128;
        buffSizes[8] = 1024 * 256;
        buffSizes[9] = 1024 * 512;
/*        buffSizes[10] = 1024 * 1024;
        buffSizes[11] = 1024 * 2048;
        buffSizes[12] = 1024 * 4096;
        buffSizes[13] = 1024 * 8192;
        buffSizes[14] = 1024 * 8192 * 2;
        buffSizes[15] = 1024 * 8192 * 4;*/

        float[] buffTimes = new float[sizeCount];

        InputStream in = null;
        OutputStream out = null;
        long precision = 1000 * 1000 * 1000;
        try {
            in = new FileInputStream(inFile);
            out = new FileOutputStream(outFile);
            copyFile(in, out, 1024);
            for (int j = 0; j < iterations; j++) {
                for (int i = 0; i < sizeCount; i++) {

                    deleteIfExists(outFile);
                    in.close();
                    out.close();
                    in = new FileInputStream(inFile);
                    out = new FileOutputStream(outFile);
                    execTime = System.nanoTime();
                    copyFile(in, out, buffSizes[i]);
                    buffTimes[i] += (float) (System.nanoTime() - execTime) / (float) precision;
                    System.out.println("for buff size " + Integer.toString(buffSizes[i]) + " time is " + Float.toString((float) (System.nanoTime() - execTime) / (float) precision));
                }
                for (int i = sizeCount - 1; i > -1; i--) {
                    deleteIfExists(outFile);
                    in.close();
                    out.close();
                    in = new FileInputStream(inFile);
                    out = new FileOutputStream(outFile);
                    execTime = System.nanoTime();
                    copyFile(in, out, buffSizes[i]);
                    buffTimes[i] += (float) (System.nanoTime() - execTime) / (float) precision;
                    System.out.println("for buff size " + Integer.toString(buffSizes[i]) + " time is " + Float.toString((float) (System.nanoTime() - execTime) / (float) precision));
                }
            }

            for(int k = 0; k < sizeCount; k++) {
                System.out.println("for buff size " + Integer.toString(buffSizes[k]) + " average time is " + Float.toString(buffTimes[k] / (float) (iterations*2)));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (in != null) in.close();
                if (out != null) out.close();
            } catch (IOException e) {
                System.out.println("exception on in and out buffers close attempt - " + e.getMessage());
            }
            //deleteIfExists(outFile);
        }

    }

    private static void copyFile(InputStream in, OutputStream out, int buffSize) throws IOException {
        byte[] buff = new byte[buffSize];
        while (in.read(buff) > 0) {
            out.write(buff);
            // out.flush();
        }
    }

    private static void deleteIfExists(String path) {
        File hFile = new File(path);
        if (hFile.exists()) {
            hFile.delete();
        }
    }
}
