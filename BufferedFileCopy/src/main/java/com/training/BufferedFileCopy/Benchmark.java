package com.training.BufferedFileCopy;

import java.io.*;

/**
 * Created by andrew on 15.12.14.
 */
public class Benchmark {
    public static void main(String[] args) {
        int minBuffSize = 1024;
        int maxBuffSize = 1024*16;
        int buffSize = 1024;
        int step = 1024;
        InputStream in = null;
        OutputStream out = null;
        long prevTime = 0;
        long execTime;
        long avgTime;
        int statIterations = 1000;
        int statCounter = 0;
        try {
            //deleteIfExists("/home/andrew/out");
            in = new FileInputStream("/home/andrew/dst1");
            out = new FileOutputStream("/home/andrew/out");

            byte[] buff = new byte[1024*16];
            execTime = System.nanoTime();
            while(true) {
                if(in.read(buff, 0, buffSize) <= 0) break;
                out.write(buff, 0, buffSize);
                out.flush();
                statCounter++;
                if(statCounter == statIterations){
                    avgTime = (System.nanoTime() - execTime)/statIterations;

                    if(avgTime < prevTime) {
                        if(buffSize < maxBuffSize) buffSize += step;
                        System.out.println("better, increment >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                    else {
                        if(buffSize > minBuffSize) buffSize -= step;
                        System.out.println("worse, decrement <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    }
                    System.out.println("average = " + Long.toString(avgTime));
                    System.out.println("prev = " + Long.toString(prevTime));
                    prevTime = avgTime;
                    statCounter = 0;
                    System.out.println("buff size = " + Integer.toString(buffSize));
                    execTime = System.nanoTime();
                }

            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally {
            try {
                    if(in != null) in.close();
                    if(out != null) out.close();
            }
            catch(IOException e) {
                System.out.println("exception on in and out buffers close attempt - " + e.getMessage());
            }

        }

    }

    private static void  deleteIfExists(String path) {
        File hFile = new File(path);
        if(hFile.exists()) {
            hFile.delete();
        }
    }
}
