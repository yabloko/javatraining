package com.training.db.interfaces;

import com.training.db.models.Table;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by andrew on 05.02.15.
 */
public interface DbStructureScanner {
    /**
     * scans database and stores basic structure in
     *
     * @param conn opened Connection to database
     * @return List of Table objects in given database
     *  @throws java.sql.SQLException in case of some problems accessing to DB
     *  via ResultSetMetaData or vendor-specific metadata tables
     */
    public List<Table> getStructure(Connection conn) throws SQLException;
}
