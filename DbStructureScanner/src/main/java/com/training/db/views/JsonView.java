package com.training.db.views;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by andrew on 06.02.15.
 */
public class JsonView {
    public static void toStdOut(Object obj){
        Gson gson = new GsonBuilder().create();
        System.out.println(gson.toJson(obj));
    }
}
