package com.training.db.components;

import com.training.db.interfaces.DbStructureScanner;
import com.training.db.models.Column;
import com.training.db.models.Table;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by andrew on 05.02.15.
 */
public class InformationSchemaDbStructureScanner implements DbStructureScanner {
    @Override
    public List<Table> getStructure(Connection conn) throws SQLException {
            ResultSet res = conn.createStatement().executeQuery("SELECT table_name, table_schema FROM information_schema.tables WHERE table_schema = 'public';");
            List<Table> tables = new LinkedList<>();
            while(res.next()) {
                Table table = new Table(res.getString("table_name"), res.getString("table_schema"));
                List<Column> columns = new LinkedList<>();
                ResultSet subset = conn.createStatement().executeQuery("SELECT column_name, data_type FROM information_schema.columns WHERE table_name = '" + res.getString("table_name") + "';");
                while (subset.next()) {
                    columns.add(new Column(subset.getString("column_name"), subset.getString("data_type")));
                }
                table.setColumns(columns);
                tables.add(table);
            }
            return tables;
    }
}
