package com.training.db.components;

import com.training.db.interfaces.DbStructureScanner;
import com.training.db.models.Column;
import com.training.db.models.ForeignKey;
import com.training.db.models.Index;
import com.training.db.models.Table;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by andrew on 05.02.15.
 */
public class MetaDataDbStructureScanner implements DbStructureScanner {
    @Override
    public List<Table> getStructure(Connection conn) throws SQLException {
        DatabaseMetaData dbMetaData = conn.getMetaData();
        ResultSet rs = dbMetaData.getTables(null, null, "%", new String[]{"TABLE"});
        List<Table> tables = new LinkedList<>();
        while (rs.next()) {
            Table table = new Table(rs.getString("TABLE_NAME"), rs.getString("TABLE_SCHEM"));
            List<Column> columns = new LinkedList<>();
            List<ForeignKey> fKeys = new LinkedList<>();
            List<Index> indexes = new LinkedList<>();
            ResultSet columnsSet = dbMetaData.getColumns(null, rs.getString("TABLE_SCHEM"), rs.getString("TABLE_NAME"), "%");
            ResultSet foreignSet = dbMetaData.getExportedKeys(null, rs.getString("TABLE_SCHEM"), rs.getString("TABLE_NAME"));
            ResultSet indexSet = dbMetaData.getIndexInfo(null, rs.getString("TABLE_SCHEM"), rs.getString("TABLE_NAME"), false, true);

            while(indexSet.next()){
                indexes.add(new Index(indexSet.getString("INDEX_NAME"), indexSet.getString("COLUMN_NAME"), indexSet.getString("TYPE")));
            }
            while (foreignSet.next()) {
                fKeys.add(new ForeignKey(foreignSet.getString("FK_NAME"), foreignSet.getString("PKCOLUMN_NAME"), foreignSet.getString("FKTABLE_NAME"), foreignSet.getString("FKCOLUMN_NAME")));
            }
            while (columnsSet.next()) {
                columns.add(new Column(columnsSet.getString("COLUMN_NAME"), columnsSet.getString("TYPE_NAME")));
            }
            table.setForeignKeys(fKeys);
            table.setColumns(columns);
            table.setIndexes(indexes);
            tables.add(table);
        }
        return tables;
    }
}
