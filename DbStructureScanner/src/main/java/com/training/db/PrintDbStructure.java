package com.training.db;

import com.training.db.components.MetaDataDbStructureScanner;
import com.training.db.interfaces.DbStructureScanner;
import com.training.db.views.JsonView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by andrew on 05.02.15.
 */
public class PrintDbStructure {
    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/javatraining", "java_user", "ibah$4")) {
            DbStructureScanner scan = //new InformationSchemaDbStructurePrinter();
            new MetaDataDbStructureScanner();
            //h2 - inmemory db
            JsonView.toStdOut(scan.getStructure(conn));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
