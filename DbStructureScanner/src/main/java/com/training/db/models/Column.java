package com.training.db.models;

/**
 * Created by andrew on 05.02.15.
 */
public class Column {
    private String name;
    private String dataType;

    public Column(String cname, String cDataType) {
        name = cname;
        dataType = cDataType;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
