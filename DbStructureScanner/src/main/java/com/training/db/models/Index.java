package com.training.db.models;

/**
 * Created by andrew on 11.02.15.
 */
public class Index {
    private String name;
    private String columnName;
    private String type;

    public Index(String name, String columnName, String type) {
        this.name = name;
        this.columnName = columnName;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
