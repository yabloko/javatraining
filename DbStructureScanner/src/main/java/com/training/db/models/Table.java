package com.training.db.models;

import java.util.List;

/**
 * Created by andrew on 05.02.15.
 */
public class Table {
    private String tableName;
    private List<Column> columns;
    private String schemaName;
    private List<ForeignKey> foreignKeys;
    private List<Index> indexes;
    //TODO ad info about indexes and FKs

    public Table(String tName, String sName) {
        tableName = tName;
        schemaName = sName;
    }

    public Table(String tName, String sName, List<Column> cols) {
        tableName = tName;
        schemaName = sName;
        columns = cols;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public List<ForeignKey> getForeignKeys() {
        return foreignKeys;
    }

    public void setForeignKeys(List<ForeignKey> foreignKeys) {
        this.foreignKeys = foreignKeys;
    }

    public List<Index> getIndexes() {
        return indexes;
    }

    public void setIndexes(List<Index> indexes) {
        this.indexes = indexes;
    }
}
