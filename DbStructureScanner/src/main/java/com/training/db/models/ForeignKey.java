package com.training.db.models;

/**
 * Created by andrew on 11.02.15.
 */
public class ForeignKey {
    private String name;
    private String columnName;
    private String refersToTable;
    private String refersToColumn;

    public ForeignKey(String name, String columnName, String refersToTable, String refersToColumn) {
        this.name = name;
        this.columnName = columnName;
        this.refersToTable = refersToTable;
        this.refersToColumn = refersToColumn;
    }

    public String getRefersToTable() {
        return refersToTable;
    }

    public void setRefersToTable(String refersToTable) {
        this.refersToTable = refersToTable;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefersToColumn() {
        return refersToColumn;
    }

    public void setRefersToColumn(String refersToColumn) {
        this.refersToColumn = refersToColumn;
    }
}
