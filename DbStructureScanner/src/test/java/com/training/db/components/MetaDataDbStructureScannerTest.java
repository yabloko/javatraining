package com.training.db.components;

import com.training.db.interfaces.DbStructureScanner;
import com.training.db.models.Column;
import com.training.db.models.Table;
import com.training.db.views.JsonView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class MetaDataDbStructureScannerTest {

    private Connection conn = null;

    @Before
    public void setUp() throws Exception {
        conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
        conn.createStatement().execute("DROP TABLE IF EXISTS Author;");
        conn.createStatement().execute("CREATE TABLE Author (id integer PRIMARY KEY auto_increment, firstName varchar(255), lastName varchar(255), penName varchar(255));");
        conn.createStatement().execute("DROP TABLE IF EXISTS Book;");
        conn.createStatement().execute("CREATE TABLE Book (id integer PRIMARY KEY auto_increment, title varchar(255), isbn varchar(255), authorId integer);");
    }

    @After
    public void tearDown() throws Exception {
        if (conn != null) {
            conn.close();
        }
    }

    @Test
    public void testGetStructure() throws Exception {
        List<Table> expectedTables = new LinkedList<>();
        expectedTables.add(new Table("AUTHOR", "PUBLIC",
                        Arrays.asList(new Column("ID", "INTEGER"),
                                new Column("FIRSTNAME", "VARCHAR"),
                                new Column("LASTNAME", "VARCHAR"),
                                new Column("PENNAME", "VARCHAR")
                        )
                )
        );
        expectedTables.add(new Table("BOOK", "PUBLIC",
                        Arrays.asList(new Column("ID", "INTEGER"),
                                new Column("TITLE", "VARCHAR"),
                                new Column("ISBN", "VARCHAR"),
                                new Column("AUTHORID", "INTEGER")
                        )
                )
        );
        DbStructureScanner scanner = new MetaDataDbStructureScanner();
        List<Table> actualTables = scanner.getStructure(conn);
        Column actualColumn = null;
        Table actualTable = null;
        for (Table expectedTable : expectedTables) {
            actualTable = null;
            for (Table at : actualTables) {
                if (at.getTableName().equals(expectedTable.getTableName())) actualTable = at;
            }
            assertNotNull(actualTable);
            assertEquals(actualTable.getTableName(), expectedTable.getTableName());
            assertEquals(actualTable.getSchemaName(), expectedTable.getSchemaName());
            assertEquals(actualTable.getColumns().size(), expectedTable.getColumns().size());

            for(Column expectedColumn : expectedTable.getColumns()){
                actualColumn = null;
                for(Column c : actualTable.getColumns()){
                    if(expectedColumn.getName().equals(c.getName())) actualColumn = c;
                }
                assertNotNull(actualColumn);
                assertEquals(actualColumn.getName(), expectedColumn.getName());
                assertEquals(actualColumn.getDataType(), expectedColumn.getDataType());
            }
        }
    }
}
