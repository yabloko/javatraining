package com.unittest.collections;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;


public class MyLinkedListTest {

    private LinkedList<String> tCollection = null;

    @Before
    public void setUp(){
        this.tCollection = new LinkedList<String>();
        tCollection.add("first");
        tCollection.add("second");
    }

    @Test
    public void testGetFirst() throws Exception {
        assertEquals("first", tCollection.getFirst());
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetFirstNoSuchElementException() throws Exception {
        LinkedList emptyList = new LinkedList<Integer>();
        emptyList.getFirst();
    }

    @Test(expected = NoSuchElementException.class)
    public void testGetLastNoSuchElementException() throws Exception {
        LinkedList emptyList = new LinkedList();
        emptyList.getLast();
    }

    @Test
    public void testGetLast() throws Exception {
        assertEquals("second", tCollection.getLast());
    }

    @Test
    public void testAddFirst() throws Exception {
        tCollection.addFirst("zero");
        assertEquals("zero", tCollection.getFirst());
    }

    @Test
    public void testAddLast() throws Exception {
        tCollection.addLast("third");
        assertEquals("third", tCollection.getLast());
    }

    @Test
    public void testContains() throws Exception {
        assertTrue(tCollection.contains("first"));
        assertFalse(tCollection.contains("random"));
    }

    @Test
    public void testContainsAll() throws Exception {
        LinkedList<String> toAdd = new LinkedList<String>();
        toAdd.add("adderOne");
        toAdd.add("addedTwo");
        assertFalse(tCollection.containsAll(toAdd));
        tCollection.addAll(toAdd);
        assertTrue(tCollection.containsAll(toAdd));
    }

    @Test
    public void testSize() throws Exception {
        tCollection.add("third");
        assertEquals(3, tCollection.size());
    }

    @Test
    public void testAdd() throws Exception {
        assertTrue(tCollection.add("random"));
        assertTrue(tCollection.contains("random"));
    }

    @Test
    public void testGet() throws Exception {
        assertEquals("second", tCollection.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexOutOfBoundsException() throws Exception {
       tCollection.get(2);
    }

    @Test
    public void testSet() throws Exception {
        String replaced = tCollection.set(0, "zero");
        assertEquals(tCollection.get(0), "zero");
        assertEquals(replaced, "first");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexOutOfBoundsException() throws Exception {
        tCollection.set(2, "third");
    }

    @Test
    public void testAddIndex() throws Exception {
        tCollection.add(0, "zero");
        assertEquals("zero", tCollection.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testAddIndexIndexOutOfBoundsException() throws Exception {
        tCollection.add(3, "random");
    }

    @Test
    public void testRemoveObject() throws Exception {
        tCollection.add("first");
        tCollection.remove("first");
        assertNotEquals(tCollection.get(0), "first");
        assertTrue(tCollection.contains("first"));
    }

    @Test
    public void testRemoveFirst() throws Exception {
        String removed = tCollection.removeFirst();
        assertNotEquals("first", tCollection.getFirst());
        assertEquals(removed, "first");
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveFirstNoSuchElementException() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        emptyList.removeFirst();
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveLastNoSuchElementException() throws Exception {
        LinkedList<String> emptyList = new LinkedList<String>();
        emptyList.removeLast();
    }

    @Test
    public void testRemoveLast() throws Exception {
        String removed = tCollection.removeLast();
        assertNotEquals("second", tCollection.getLast());
        assertEquals(removed, "second");
    }


    @Test
    public void testRemoveIndex() throws Exception {
        String removed = tCollection.remove(0);
        assertNotEquals(tCollection.get(0), "first");
        assertEquals(removed, "first");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexIndexOutOfBoundsException() throws Exception {
        tCollection.remove(2);
       }

    @Test
    public void testRemove() throws Exception {
        String removed = tCollection.remove();
        assertNotEquals(tCollection.get(0), "first");
        assertEquals(removed, "first");
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveNoSuchElementException() throws Exception {
        LinkedList emplyList = new LinkedList();
        emplyList.remove();
    }

    @Test
    public void testRemoveFirstOccurence() throws Exception {
        tCollection.add("first");
        tCollection.removeFirstOccurrence("first");
        assertNotEquals(tCollection.get(0), "first");
        assertTrue(tCollection.contains("first"));
    }

    @Test
    public void testRemoveLastOccurence() throws Exception {
        tCollection.add("first");
        tCollection.removeLastOccurrence("first");
        assertNotEquals(tCollection.get(tCollection.size()-1), "first");
        assertTrue(tCollection.contains("first"));
    }

    @Test
    public void testRemoveAll() throws Exception {
        LinkedList<String> toRemove = new LinkedList<String>();
        toRemove.add("first");
        toRemove.add("second");
        tCollection.removeAll(toRemove);
        assertTrue(tCollection.isEmpty());
    }

    @Test
    public void testRetainAll() throws Exception {
        tCollection.retainAll(new LinkedList<String>());
        assertTrue(tCollection.isEmpty());
    }

    @Test
    public void testAddAll() throws Exception {
        LinkedList<String> container = new LinkedList<String>();
        container.addAll(tCollection);
        for(int i = 0; i < container.size(); i++)
        {
            assertEquals(tCollection.get(i),container.get(i));
        }
    }

    @Test
    public void testAddAllIndex() throws Exception {
        int offset = 1;
        LinkedList<String> container = new LinkedList<String>();
        container.add("minus one");
        container.add("zero");
        container.addAll(offset, tCollection);
        for(int i = 0; i < tCollection.size(); i++)
        {
            assertEquals(tCollection.get(i),container.get(i + offset));
        }
    }

    @Test( expected = IndexOutOfBoundsException.class)
    public void testAddAllIndexIndexOutOfBoundsException() throws Exception {
        LinkedList<String> container = new LinkedList<String>();
        container.addAll(2,tCollection);
    }

    @Test( expected = NullPointerException.class)
    public void testAddAllNullPointerException() throws Exception {
       tCollection.addAll(null);
    }

    @Test( expected = NullPointerException.class)
    public void testAddAllIndexNullPointerException() throws Exception {
        tCollection.addAll(0, null);
    }

    @Test
    public void testClear() throws Exception {
        tCollection.clear();
        assertEquals(0, tCollection.size());
    }

    @Test
    public void testDescendingIterator() throws Exception {
        Iterator<String> iterator = tCollection.descendingIterator();
        int size = tCollection.size();
        while (iterator.hasNext()){
            assertEquals(iterator.next(), tCollection.get(size-1));
            size--;
        }
    }

    @Test
    public void testElement() throws Exception {
        assertEquals(tCollection.element(), "first");
        assertEquals(tCollection.get(0), "first");
    }

    @Test(expected = NoSuchElementException.class)
    public void testElementNoSuchElementException() throws Exception {
        LinkedList<String> emptyCollection = new LinkedList<String>();
        emptyCollection.element();
     }

    @Test
    public void testIndexOf() throws Exception {
        assertEquals(tCollection.indexOf("second"), 1);
    }

    @Test
    public void testIndexOfNotExists() throws Exception {
        assertEquals(tCollection.indexOf("random"), -1);
    }

    @Test
    public void testLastIndexOf() throws Exception {
        tCollection.add("first");
        assertEquals(tCollection.lastIndexOf("first"), 2);
    }

    @Test
    public void testLastIndexOfNotExists() throws Exception {
        assertEquals(tCollection.lastIndexOf("random"), -1);
    }

    @Test
    public void testIterator() throws Exception {
        Iterator<String> iterator = tCollection.iterator();
        int i = 0;
        while(iterator.hasNext())
        {
            assertEquals(iterator.next(), tCollection.get(i));
            i++;
        }
    }

    @Test
    public void testListIterator() throws Exception {
        tCollection.add("third");
        tCollection.add("fourth");
        int offset = 2;
        Iterator<String> iterator = tCollection.listIterator(offset);
        int i = offset;
        while(iterator.hasNext())
        {
            assertEquals(iterator.next(), tCollection.get(i));
            i++;
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListIteratorIndexOutOfBoundsException() throws Exception {
        tCollection.listIterator(4);
    }

    @Test
    public void testOffer() throws Exception {
        tCollection.offer("third");
        assertEquals(tCollection.get(tCollection.size() - 1), "third");
    }

    @Test
    public void testOfferFirst() throws Exception {
        tCollection.offerFirst("zero");
        assertEquals(tCollection.get(0), "zero");
    }

    @Test
    public void testOfferLast() throws Exception {
        tCollection.offer("third");
        assertEquals(tCollection.get(tCollection.size() - 1), "third");
    }

    @Test
    public void testPeek() throws Exception {
        assertEquals(tCollection.peek(), "first");
        assertEquals(tCollection.get(0), "first");
    }

    @Test
    public void testPeekFirst() throws Exception {
        assertEquals(tCollection.peekFirst(), "first");
        assertEquals(tCollection.get(0), "first");
    }

    @Test
    public void testPeekLast() throws Exception {
        assertEquals(tCollection.peekLast(), "second");
        assertEquals(tCollection.get(tCollection.size() - 1), "second");
    }

    @Test
    public void testPoll() throws Exception {
        String polled = tCollection.poll();
        assertEquals(polled, "first");
        assertNotEquals(tCollection.get(0), "first");
        tCollection.poll();
        assertNull(tCollection.poll());
    }

    @Test
    public void testPollFirst() throws Exception {
        String polled = tCollection.pollFirst();
        assertEquals(polled, "first");
        assertNotEquals(tCollection.get(0), "first");
        tCollection.pollFirst();
        assertNull(tCollection.pollFirst());
    }

    @Test
    public void testPollLast() throws Exception {
        String polled = tCollection.pollLast();
        assertEquals(polled, "second");
        assertNotEquals(tCollection.get(tCollection.size()-1), "second");
        tCollection.pollLast();
        assertNull(tCollection.pollLast());
    }

    @Test
    public void testPop() throws Exception {
        String popped = tCollection.pop();
        assertEquals(popped, "first");
        assertNotEquals(tCollection.get(0), "first");
    }

    @Test(expected = NoSuchElementException.class)
    public void testPopNoSuchElementException() throws Exception {
        tCollection.pop();
        tCollection.pop();
        tCollection.pop();
    }

    @Test
    public void testPush() throws Exception {
        tCollection.push("zero");
        assertEquals(tCollection.get(0), "zero");
    }

    @Test
    public void testIsEmpty() throws Exception {
        LinkedList list = new LinkedList();
        assertTrue(list.isEmpty());
        assertFalse(tCollection.isEmpty());
    }

    @Test
    public void testEquals() throws Exception {
        LinkedList<String> toCompare = new LinkedList<String>();
        toCompare.add("first");
        toCompare.add("second");
        assertTrue(tCollection.equals(toCompare));
        assertFalse(tCollection.equals(new LinkedList<String>()));
    }
}