package com.training.ExceptionBenchmark;

import java.util.Collections;

/**
 * Created by andrew on 02.12.14.
 */
public class Benchmark {
    public static void main(String[] args) {
        int i;
        int iterations = 100000;
        String exception = "";
        Doer doer = new Doer();
        long execTime = 0;
        long exceptionTime = 0;
        long returnTime = 0;
        long sortTime = 0;
        int benchIterations = 5;
        //warmup
        doer.refillList();
        for (i = 0; i < iterations; i++) {
            try {
                doer.doSomethingException();
            } catch (OddException e) {
                Collections.shuffle(doer.list);
            }
        }
        for (i = 0; i < iterations; i++) {
            if (doer.doSomethingReturn() == -1) {
                Collections.shuffle(doer.list);
            }
        }
        //bench
        for (int j = 0; j < benchIterations; j++) {
            sortTime = 0;
            execTime = System.nanoTime();
            for (i = 0; i < iterations; i++) {
                try {
                    doer.doSomethingException();
                } catch (OddException e) {
                    exception = e.getMessage();
                    sortTime += doer.sortTime;
                    Collections.shuffle(doer.list);
                }
            }

            exceptionTime = (System.nanoTime() - execTime) - sortTime;

            //System.out.printf("exception exec time is %3d\n", exceptionTime);
            //System.out.printf("exception sort time is %3d\n", sortTime);

            execTime = System.nanoTime();
            sortTime = 0;
            for (i = 0; i < iterations; i++) {
                if (doer.doSomethingReturn() == -1) {
                    sortTime += doer.sortTime;
                    Collections.shuffle(doer.list);
                }
            }

            returnTime = (System.nanoTime() - execTime) - sortTime;

            //System.out.printf("return exec time is %3d\n", returnTime);
            //System.out.printf("return sort time is %3d\n", sortTime);

            float result = (float)exceptionTime/(float)returnTime;
            System.out.print("Exception is " + String.format("%3f", result) + " times slower\n");

        }
        Collections.shuffle(doer.list);
        System.out.print(exception + "\n");
    }
}
