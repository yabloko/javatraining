package com.training.ExceptionBenchmark;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by andrew on 02.12.14.
 */
public class Doer {
    //TODO
    /*
        add heavy calculations instead of flip boolean - array shuffle and sort is OK
        call sort result in the outside to guarantee execution of code
        get rid of random, throw exception each time iteration executed
        measure overall time and substitute time of load function execution
     */

    public List<String> list = null;
    private int listSize = 16;
    public long sortTime = 0;

    public void refillList() {
        if (list == null) {
            list = new LinkedList<String>();
        }
        for (int i = 0; i < listSize; i++) {
            list.add(UUID.randomUUID().toString());
        }
    }

    public void doSomethingException() throws OddException {
        long execTime = System.nanoTime();
        list.sort(new StringComparator());
        sortTime = System.nanoTime() - execTime;
        throw new OddException();
    }

    public int doSomethingReturn() {
        long execTime = System.nanoTime();
        list.sort(new StringComparator());
        sortTime = System.nanoTime() - execTime;
        return -1;
    }

    private class StringComparator implements Comparator<String> {

        public int compare(String o1, String o2) {
            if (o1 == o2)
                return 0;
            if (o1 == null)
                return 1;
            if (o2 == null)
                return -1;
            return o1.compareTo(o2);
        }

    }
}


