package com.training.streamtutorial;

import com.training.streamtutorial.enums.Gender;
import com.training.streamtutorial.interfaces.CollectionProcessing;
import com.training.streamtutorial.model.Group;
import com.training.streamtutorial.model.Mark;
import com.training.streamtutorial.model.Student;
import org.junit.Before;
import org.junit.Test;

import javax.swing.text.html.HTMLDocument;
import java.util.*;

import static org.junit.Assert.*;

public class CollectionProcessorTest {

    private List<Group> groups;
    private CollectionProcessing processor;

    @Before
    public void initTest() {
        processor = new StreamAPI();
        groups = new LinkedList<>();
        List<Student> studentsOfGroupOne = new LinkedList<>();
        List<Mark> marksStudentOfGroupOne_1 = new LinkedList<>();
        marksStudentOfGroupOne_1.add(new Mark("Chemistry", 3));
        marksStudentOfGroupOne_1.add(new Mark("Physics", 4));

        List<Mark> marksStudentOfGroupOne_2 = new LinkedList<>();
        marksStudentOfGroupOne_2.add(new Mark("Chemistry", 5));
        marksStudentOfGroupOne_2.add(new Mark("Physics", 2));

        studentsOfGroupOne.add(new Student("John", "Doe", "", Gender.MALE, 19, marksStudentOfGroupOne_1));
        studentsOfGroupOne.add(new Student("Jane", "Black", "", Gender.FEMALE, 20, marksStudentOfGroupOne_2));

        groups.add(new Group("group1", 1, studentsOfGroupOne));

        List<Student> studentsOfGroupTwo = new LinkedList<>();
        List<Mark> marksStudentOfGroupTwo_1 = new LinkedList<>();

        marksStudentOfGroupTwo_1.add(new Mark("Math", 5));
        marksStudentOfGroupTwo_1.add(new Mark("English", 5));

        List<Mark> marksStudentOfGroupTwo_2 = new LinkedList<>();
        marksStudentOfGroupTwo_2.add(new Mark("Math", 5));
        marksStudentOfGroupTwo_2.add(new Mark("English", 5));

        studentsOfGroupTwo.add(new Student("Jack", "Smith", "", Gender.MALE, 17, marksStudentOfGroupTwo_1));
        studentsOfGroupTwo.add(new Student("John", "Smith", "", Gender.MALE, 21, marksStudentOfGroupTwo_2));

        groups.add(new Group("group2", 2, studentsOfGroupTwo));
    }

    @Test
    public void testFindGroupsWithLowResults() throws Exception {
        assertEquals(1, processor.findGroupsWithLowResults(groups));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindGroupsWithLowResultsIllegalArgumentException() throws Exception {
        processor.findGroupsWithLowResults(null);
    }

    @Test
    public void testGetAverageMarkPerGroup() throws Exception {
        Map<String, Float> expectedAverageMarks = new HashMap<>();
        expectedAverageMarks.put("group1", new Float(3.5));
        expectedAverageMarks.put("group2", new Float(5));
        Map<String, Float> averageMarks = processor.getAverageMarkPerGroup(groups);
        Iterator it = expectedAverageMarks.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Float> entry = (Map.Entry)it.next();
            assertEquals(entry.getValue(), averageMarks.get(entry.getKey()));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAverageMarkPerGroupIllegalArgumentException() throws Exception {
        processor.getAverageMarkPerGroup(null);
    }

    @Test
    public void testGetAverageMarkPerSubject() throws Exception {
        Map<String, Float> expectedAverageMarks = new HashMap<>();
        expectedAverageMarks.put("Chemistry", new Float(4));
        expectedAverageMarks.put("Physics", new Float(3));
        expectedAverageMarks.put("English", new Float(5));
        expectedAverageMarks.put("Math", new Float(5));
        Map<String, Float> averageMarks = processor.getAverageMarkPerSubject(groups);
        Iterator it = expectedAverageMarks.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Float> entry = (Map.Entry)it.next();
            assertEquals(entry.getValue(), averageMarks.get(entry.getKey()));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAverageMarkPerSubjectIllegalArgumentException() throws Exception {
        processor.getAverageMarkPerSubject(null);
    }

    @Test
    public void testGetOnlyMaleGroupNames() throws Exception {
        List<String> expectedOnlyMaleGroups = Arrays.asList("group2");
        List<String> onlyMaleGroups = processor.getOnlyMaleGroupNames(groups);
        assertEquals(expectedOnlyMaleGroups.size(), onlyMaleGroups.size());
        assertTrue(expectedOnlyMaleGroups.containsAll(onlyMaleGroups));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOnlyMaleGroupNamesIllegalArgumentException() throws Exception {
        processor.getOnlyMaleGroupNames(null);
    }


    @Test
    public void testGetAllStudentNames() throws Exception {
        List<String> expectedNames = Arrays.asList("John Doe", "Jane Black", "John Smith", "Jack Smith");
        List<String> names = processor.getAllStudentNames(groups);
        assertEquals(4, names.size());
        assertTrue(expectedNames.containsAll(names));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAllStudentNamesIllegalArgumentException() throws Exception {
        processor.getAllStudentNames(null);
    }

    @Test
    public void testGetStudentsOfAgeWithin() throws Exception {
        List<String> expectedNames = Arrays.asList("John Doe");
        List<String> names = processor.getStudentsOfAgeWithin(groups, 18, 20);
        assertEquals("Expected ", expectedNames.size(), names.size());
        assertTrue(expectedNames.containsAll(names));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStudentsOfAgeWithinIllegalArgumentExceptionNullGroup() throws Exception {
        processor.getStudentsOfAgeWithin(null, 1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStudentsOfAgeWithinIllegalArgumentExceptionNegativeMin() throws Exception {
        processor.getStudentsOfAgeWithin(groups, -1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStudentsOfAgeWithinIllegalArgumentExceptionMinLargerThanMax() throws Exception {
        processor.getStudentsOfAgeWithin(groups, 2, 1);
    }

    @Test
    public void testGetSubjects() throws Exception {
        List<String> expectedSubjects = Arrays.asList("Chemistry", "English", "Math", "Physics");
        List<String> subjects = processor.getSubjects(groups);
        assertEquals(expectedSubjects.size(), subjects.size());
        assertTrue(expectedSubjects.containsAll(subjects));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSubjectsIllegalArgumentException() throws Exception {
        processor.getSubjects(null);
    }

    @Test
    public void testGetGroupsWithAtLeastStudentsWithOnlyMarks() throws Exception {
        List<String> expectedGroupNames = Arrays.asList("group1", "group2");
        List<String> groupNames = processor.getGroupsWithAtLeastStudentsWithOnlyMarks(groups, 1, 2);
        assertEquals("Actual count of group names doesnt match expected", expectedGroupNames.size(), groupNames.size());
        assertTrue("Actual grops names doesnt match expected", expectedGroupNames.containsAll(groupNames));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetGroupsWithAtLeastStudentsWithOnlyMarksIllegalArgumentExceptionNullGroup() throws Exception {
        processor.getGroupsWithAtLeastStudentsWithOnlyMarks(null, 1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetGroupsWithAtLeastStudentsWithOnlyMarksIllegalArgumentExceptionNegativeNum() throws Exception {
        processor.getGroupsWithAtLeastStudentsWithOnlyMarks(groups, 0, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetGroupsWithAtLeastStudentsWithOnlyMarksIllegalArgumentExceptionNegativeMark() throws Exception {
        processor.getGroupsWithAtLeastStudentsWithOnlyMarks(groups, 2, 0);
    }
}