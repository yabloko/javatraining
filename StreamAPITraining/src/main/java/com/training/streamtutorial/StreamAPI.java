package com.training.streamtutorial;

import com.training.streamtutorial.enums.Gender;
import com.training.streamtutorial.interfaces.CollectionProcessing;
import com.training.streamtutorial.model.Group;
import com.training.streamtutorial.model.Mark;
import com.training.streamtutorial.model.Student;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by andrew on 22.01.15.
 */
public class StreamAPI implements CollectionProcessing {

    @Override
    public int findGroupsWithLowResults(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        return (int) groups.stream().filter(
                g -> g.getStudents().stream().filter(
                        s -> s.getMarks().stream().filter(
                                m -> m.getMark() < 3).findFirst().isPresent()
                ).findFirst().isPresent()
        ).count();
    }

    @Override
    public Map<String, Float> getAverageMarkPerGroup(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException();

        return groups.stream().map(
                g -> new Tuple<String, OptionalDouble>(
                        g.getName(),
                        g.getStudents()
                                .stream()
                                .flatMap(s -> s.getMarks().stream())
                                .mapToInt(m -> m.getMark()).average()
                )).collect(Collectors.toMap(t -> t.getKey(), t -> new Float(t.getValue().getAsDouble())));
    }

    @Override
    public List<String> getOnlyMaleGroupNames(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        List<String> names = groups.stream().filter(
                g -> g.getStudents().stream().allMatch(
                        s -> s.getGender() == Gender.MALE
                )
        ).map(Group::getName).collect(Collectors.toList());
        return names;
    }

    @Override
    public List<String> getAllStudentNames(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        return groups.stream().flatMap(
                g -> g.getStudents().stream()
        ).map(
                s -> s.getFirstName() + " " + s.getLastName()
        ).collect(Collectors.toList());
    }

    @Override
    public Map<String, Float> getAverageMarkPerSubject(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        /*Consumer<String> f = System.out::println;
        f.accept("sgr");

        BiConsumer<Mark, String> c1 = Mark::setSubjectName;
        c1.accept(new Mark("1", 1), "2");

        Function<Mark, String> f1 = Mark::getSubjectName;
        f1.apply(new Mark("1", 1));
*/
        return groups.stream().
                flatMap(
                        g -> g.getStudents().stream()
                )
                .flatMap(
                        s -> s.getMarks().stream()
                ).collect(
                        Collectors.groupingBy(
                                    Mark::getSubjectName, Collectors.collectingAndThen(
                                        Collectors.averagingInt(Mark::getMark), res ->  new Float(res)
                                )
                        )
                );
    }


    @Override
    public List<String> getStudentsOfAgeWithin(List<Group> groups, int min, int max) throws IllegalArgumentException {
        if(groups == null || min < 1 || max < min) throw new IllegalArgumentException();
        return groups.stream().flatMap(
                g -> g.getStudents().stream()).filter(
                s -> (s.getAge() > min && s.getAge() < max && s.getGender() == Gender.MALE)).map(
                s -> s.getFirstName() + " " + s.getLastName()).collect(Collectors.toList());
    }

    @Override
    public List<String> getSubjects(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        return groups
                .stream()
                .flatMap(g -> g.getStudents().stream())
                .flatMap(s -> s.getMarks().stream())
                .map(m -> m.getSubjectName())
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getGroupsWithAtLeastStudentsWithOnlyMarks(List<Group> groups, int num, int mark) throws IllegalArgumentException {
        if(groups == null || num < 1 || mark < 1) throw new IllegalArgumentException();
        return groups.stream().filter(
                g -> g.getStudents().stream().filter(
                        s -> s.getMarks().stream().allMatch(
                                m -> m.getMark() >= mark)).count() > num).map(
                g -> g.getName()).collect(Collectors.toList());
    }

    class Tuple<K,V> {
        private K key;
        private V value;

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public Tuple(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
