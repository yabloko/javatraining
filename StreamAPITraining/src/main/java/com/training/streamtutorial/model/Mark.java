package com.training.streamtutorial.model;

/**
 * Created by andrew on 22.01.15.
 */
public class Mark {

    protected String subjectName;
    protected int mark;

    public Mark(String subjectName, int mark) throws IllegalArgumentException {
        if(subjectName == null || mark < 1) throw new IllegalArgumentException("Error constructing mark");
        this.subjectName = subjectName;
        this.mark = mark;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getMark() {
        return mark;
    }
}
