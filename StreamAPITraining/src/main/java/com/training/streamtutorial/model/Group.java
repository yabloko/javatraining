package com.training.streamtutorial.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by andrew on 22.01.15.
 */
public class Group {

    protected String name;
    protected int year;
    protected List<Student> students;

    public Group(String name, int year, List<Student> students) throws IllegalArgumentException {
        if(name == null || year < 1 || students == null)
            throw new IllegalArgumentException("Error construction group");
        this.name = name;
        this.year = year;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public List<Student> getStudents() {
        return students;
    }

}
