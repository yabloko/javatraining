package com.training.streamtutorial.model;

import com.training.streamtutorial.enums.Gender;

import java.util.List;

/**
 * Created by andrew on 22.01.15.
 */
public class Student {

    protected String firstName;
    protected String lastName;
    protected String middleName;
    protected Gender gender;
    protected int age;
    protected List<Mark> marks;

    public Student(String firstName, String lastName, String middleName, Gender gender, int age, List<Mark> marks) {
        //FFFUUUU long case but just get over it
        if(firstName == null || lastName == null || gender == null || age < 1 || marks == null)
            throw new IllegalArgumentException("Error constructing student");
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.gender = gender;
        this.age = age;
        this.marks = marks;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getMiddleName() {
        return middleName;
    }


    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public List<Mark> getMarks() {
        return marks;
    }

}
