package com.training.streamtutorial;

import com.training.streamtutorial.enums.Gender;
import com.training.streamtutorial.interfaces.CollectionProcessing;
import com.training.streamtutorial.model.Group;
import com.training.streamtutorial.model.Mark;
import com.training.streamtutorial.model.Student;

import java.util.*;

/**
 * Created by andrew on 22.01.15.
 */
public class NoStreamAPI implements CollectionProcessing {

    public int findGroupsWithLowResults(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException("group list is null");
        int count = 0;
        for (Group gr : groups) {
            if (isGroupHasStudentWithResultLowerThan(gr, 3)) count++;
        }
        return count;

    }

    public Map<String, Float> getAverageMarkPerGroup(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException("group list is null");
        Map<String, Float> avg = new HashMap<>();
        for (Group gr : groups) {
            avg.put(gr.getName(), getAverageMark(gr));
        }
        return avg;
    }

    public List<String> getOnlyMaleGroupNames(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException("group list is null");
        List<String> onlyMale = new LinkedList<String>();
        for (Group gr : groups) {
            if (isOnlyGender(Gender.MALE, gr)) onlyMale.add(gr.getName());
        }
        return onlyMale;

    }

    public List<String> getAllStudentNames(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException("group list is null");
        List<String> names = new LinkedList<String>();
        for (Group gr : groups) {
            names.addAll(getAllStudentNames(gr));
        }
        return names;
    }

    @Override
    public Map<String, Float> getAverageMarkPerSubject(List<Group> groups) throws IllegalArgumentException {
        if(groups == null) throw new IllegalArgumentException();
        Map<String, Integer> marksSum = new HashMap<>();
        Map<String, Integer> marksCount = new HashMap<>();
        Map<String, Float> averageMarks = new HashMap<>();
        for(Group gr : groups){
            for(Student st : gr.getStudents()){
                for(Mark m : st.getMarks()){
                    if(marksSum.containsKey(m.getSubjectName())) {
                        marksSum.put(m.getSubjectName(), marksSum.get(m.getSubjectName()) + m.getMark());
                        marksCount.put(m.getSubjectName(), marksCount.get(m.getSubjectName()) + 1);
                    }
                    else{
                        marksSum.put(m.getSubjectName(), m.getMark());
                        marksCount.put(m.getSubjectName(), 1);
                    }
                }
            }
        }
        Iterator<Map.Entry<String, Integer>> subjects = marksSum.entrySet().iterator();
        while(subjects.hasNext()){
            Map.Entry<String, Integer> subj = subjects.next();
            Float average = new Float(new Float(subj.getValue())/new Float(marksCount.get(subj.getKey())));
            averageMarks.put(subj.getKey(), average);
        }
        return averageMarks;
    }

    //TODO average marks for each subject

    public List<String> getStudentsOfAgeWithin(List<Group> groups, int min, int max) throws IllegalArgumentException {
        if (groups == null || min < 1 || max < min) throw new IllegalArgumentException();
        List<String> names = new LinkedList<String>();
        for (Group gr : groups) {
            names.addAll(getStudentsOfGroupOfAgeWithin(gr, min, max));
        }
        return names;

    }

    public List<String> getSubjects(List<Group> groups) throws IllegalArgumentException {
        if (groups == null) throw new IllegalArgumentException();
        List<String> subjects = new LinkedList<String>();
        for (Group gr : groups) {
            for (String name : getSubjects(gr)) {
                if (!subjects.contains(name)) subjects.add(name);
            }
        }
        return subjects;
    }

    public List<String> getGroupsWithAtLeastStudentsWithOnlyMarks(List<Group> groups, int num, int mark) throws IllegalArgumentException {
        if (groups == null || num < 1 || mark < 1) throw new IllegalArgumentException();
        List<String> names = new LinkedList<String>();
        for (Group gr : groups) {
            if (groupHasAtLeastStudentsWithOnlyMarks(gr, num, mark)) names.add(gr.getName());
        }
        return names;
    }

    private boolean groupHasAtLeastStudentsWithOnlyMarks(Group group, int num, int mark) {
        if(group == null || num < 1 || mark < 1) throw new IllegalArgumentException();
        int count = 0;
        for (Student st : group.getStudents()) {
            if (isStudentHasOnlyMarks(st, mark)) count++;
            if (count == num) return true;
        }
        return false;
    }

    private boolean isStudentHasOnlyMarks(Student st, int mark) {
        for (Mark m : st.getMarks()) {
            if (m.getMark() < mark) return false;
        }
        return true;
    }

    private List<String> getSubjects(Group group) {
        List<String> subjects = new LinkedList<>();
        for (Student st : group.getStudents()) {
            for (String name : getSubjects(st)) {
                if (!subjects.contains(name)) subjects.add(name);
            }
        }
        return subjects;
    }

    private List<String> getSubjects(Student student) {
        List<String> subjects = new LinkedList<>();
        for (Mark m : student.getMarks()) {
            if (!subjects.contains(m.getSubjectName())) subjects.add(m.getSubjectName());
        }
        return subjects;
    }

    private List<String> getStudentsOfGroupOfAgeWithin(Group gr, int min, int max) {
        List<String> names = new LinkedList<>();
        for (Student st : gr.getStudents()) {
            if (st.getAge() >= min && st.getAge() <= max && st.getGender() == Gender.MALE) {
                names.add(st.getFirstName() + " " + st.getLastName());
            }
        }
        return names;
    }

    private List<String> getAllStudentNames(Group gr) {
        List<String> names = new LinkedList<>();
        for (Student st : gr.getStudents()) {
            names.add(st.getFirstName() + " " + st.getLastName());
        }
        return names;
    }

    private boolean isOnlyGender(Gender gender, Group group) {
        for (Student st : group.getStudents()) {
            if (st.getGender() != gender) return false;
        }
        return true;
    }

    private float getAverageMark(Group gr) {
        int count = 0;
        int sum = 0;
        for (Student st : gr.getStudents()) {
            for (Mark m : st.getMarks()) {
                sum += m.getMark();
                count++;
            }
        }
        return (float) sum / (float) count;
    }

    private boolean isStudentHasResultsLowerThan(Student student, int limit) {
        for (Mark m : student.getMarks()) {
            if (m.getMark() < limit) {
                return true;
            }
        }
        return false;
    }

    private boolean isGroupHasStudentWithResultLowerThan(Group group, int limit) {
        for (Student st : group.getStudents()) {
            if (isStudentHasResultsLowerThan(st, limit)) return true;
        }
        return false;
    }
}
