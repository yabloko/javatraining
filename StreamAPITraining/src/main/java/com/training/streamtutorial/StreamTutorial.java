package com.training.streamtutorial;

import com.training.streamtutorial.interfaces.CollectionProcessing;
import com.training.streamtutorial.model.Group;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by andrew on 22.01.15.
 * temporary class just for output reference
 */
public class StreamTutorial {
    public static void main(String[] args) {
        LinkedList<Group> groups = null;
        try {
            groups = new LinkedList(GroupFactory.getGroup());
        } catch (IllegalArgumentException e) {
            //oh my god just log it
            System.out.println(e.getMessage());
        }
        if (groups != null) {
            //CollectionProcessing stream = new NoStreamAPI();
            CollectionProcessing stream = new StreamAPI();

            System.out.println("number of groups of students with low results: " + Integer.toString(stream.findGroupsWithLowResults(groups)));

            System.out.println("average mark per group:");
            Map<String, Float> averageMarkPerGroup = stream.getAverageMarkPerGroup(groups);
            Iterator it = averageMarkPerGroup.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Float> entry = (Map.Entry) it.next();
                System.out.println("average mark for group " + entry.getKey() + " is " + entry.getValue());
            }

            System.out.println("average marks per subject:");
            Map<String, Float> averageMarkPerSubject = stream.getAverageMarkPerSubject(groups);
            it = averageMarkPerSubject.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Float> entry = (Map.Entry) it.next();
                System.out.println("average mark for subject " + entry.getKey() + " is " + entry.getValue());
            }

            System.out.println("list of group names with only male students:");
            for (String name : stream.getOnlyMaleGroupNames(groups)) {
                System.out.println("\t" + name);
            }

            System.out.println("list of all student names");
            for (String name : stream.getAllStudentNames(groups)) {
                System.out.println("\t" + name);
            }

            System.out.println("spisok studentov, kotorie otpravlyautsa v voenkomat:");
            for (String name : stream.getStudentsOfAgeWithin(groups, 18, 25)) {
                System.out.println("\t" + name);
            }

            System.out.println("list of subjects:");
            for (String name : stream.getSubjects(groups)) {
                System.out.println("\t" + name);
            }

            int num = 2;
            int mark = 5;
            System.out.println("list of groups with at least " + Integer.toString(num) + " students with all marks not lower than " + Integer.toString(mark));
            for (String name : stream.getGroupsWithAtLeastStudentsWithOnlyMarks(groups, num, mark)) {
                System.out.println("\t" + name);
            }

        }
    }
}
