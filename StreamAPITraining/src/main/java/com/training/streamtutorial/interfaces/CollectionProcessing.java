package com.training.streamtutorial.interfaces;

import com.training.streamtutorial.model.Group;

import java.util.List;
import java.util.Map;

/**
 * Created by andrew on 24.01.15.
 */
public interface CollectionProcessing {

    public int findGroupsWithLowResults(List<Group> groups) throws IllegalArgumentException;

    public Map<String, Float> getAverageMarkPerGroup(List<Group> groups) throws IllegalArgumentException;

    public List<String> getOnlyMaleGroupNames(List<Group> groups) throws IllegalArgumentException;

    public List<String> getAllStudentNames(List<Group> groups) throws IllegalArgumentException;

    public Map<String, Float> getAverageMarkPerSubject(List<Group> groups) throws IllegalArgumentException;

    public List<String> getStudentsOfAgeWithin(List<Group> groups, int min, int max) throws IllegalArgumentException;

    public List<String> getSubjects(List<Group> groups) throws IllegalArgumentException;

    public List<String> getGroupsWithAtLeastStudentsWithOnlyMarks(List<Group> groups, int num, int mark) throws IllegalArgumentException;

}
