package com.training.streamtutorial.enums;

/**
 * Created by andrew on 22.01.15.
 */
public enum Gender {
    MALE, FEMALE
}
