package com.training.streamtutorial;

import com.training.streamtutorial.enums.Gender;
import com.training.streamtutorial.model.Group;
import com.training.streamtutorial.model.Mark;
import com.training.streamtutorial.model.Student;

import java.util.*;

/**
 * Created by andrew on 22.01.15.
 */
public class GroupFactory {
    public static List<Group> getGroup() throws IllegalArgumentException {
        int studentsNum = 10;
        List<Group> groups = new LinkedList<Group>();
        ArrayList<String> groupNames = new ArrayList<String>(Arrays.asList("group1","group2","group3","group4","group5"));
        Random rand = new Random();
        Gender gender;
        int groupSize = 3;
        int groupStart;
        Collections.shuffle(groupNames);
        groupStart = rand.nextInt(3);
        for(String groupName : new ArrayList<>(groupNames.subList(groupStart, groupStart + groupSize))) {
            LinkedList students = new LinkedList();
            for(int i = 0; i < studentsNum + rand.nextInt(11); i++){
                LinkedList<Mark> marks = new LinkedList<Mark>();
                for(String subject : getSubject()){
                    marks.add(new Mark(subject, getMark()));
                }
                gender = getGender();
                Student student = new Student(getFirstName(gender), getLastName(), null, getGender(), rand.nextInt(9) + 16, marks);
                students.add(student);
            }
            groups.add(new Group(groupName, rand.nextInt(5) + 1, students));
        }
        return groups;
    }

    protected static String getFirstName(Gender gender) {
        Random rand = new Random();
        ArrayList<String> names;
        ArrayList<String> maleNames = new ArrayList<>(Arrays.asList("John", "Jack", "Joe", "James"));
        ArrayList<String> femaleNames = new ArrayList(Arrays.asList("Joan", "Jodie", "Jill", "Jane"));
        if(gender == Gender.MALE) {
            names = maleNames;
        }
        else {
            names = femaleNames;
        }
        return names.get(rand.nextInt(names.size()));
    }

    protected static String getLastName() {
        Random rand = new Random();
        ArrayList<String> names = new ArrayList(Arrays.asList("Smith", "Black", "Brook", "Dean"));
        return names.get(rand.nextInt(names.size()));
    }

    protected static Gender getGender() {
        Random rand = new Random();
        if(rand.nextInt(3) == 1) return Gender.MALE;
        return Gender.FEMALE;
    }

    protected static int getMark() {
        Random rand = new Random();
        return rand.nextInt(5) + 1;
    }

    protected static List<String> getSubject() {
        Random rand = new Random();
        ArrayList<String> subjects = new ArrayList(Arrays.asList("Math", "Physics", "English", "Literature", "Applied Cryogenics"));
        Collections.shuffle(subjects);
        int start = rand.nextInt(3);
        return subjects.subList(start, start + 3);
    }
}
